jQuery(document).ready(function() {

     // Variablen-Deklaration
    // Definition der JSON-URLs
    var geoserviceURL='http://www.geoservice.apps.be.ch/geoservice1/rest/services/a42pub1/a42pub_oereb_wms_d_fk/MapServer';
    var oerebserviceURL='https://www.oereb.apps.be.ch/OerbverSvc.svc/rest/';
    var oerebProtect = jQuery('#hidden_oerebprotect').val();
    if (oerebProtect == 'public')
    {
      geoserviceURL='http://www.geoservice.apps.be.ch/geoservice1/rest/services/a42pub1/a42pub_oereb_wms_d_fk/MapServer';
    }

    // Get URL-Parameters
    var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

      for (i = 0; i < sURLVariables.length; i++) {
          sParameterName = sURLVariables[i].split('=');

          if (sParameterName[0] === sParam) {
              return sParameterName[1] === undefined ? true : sParameterName[1];
          }
      }
    };

    var isLang = getUrlParameter('lang');

    // Restliche Variabeln
    var isalive=true;
    var inputDisable=false;
    var grundstuecknummer='';

    // Sprache
    if (isLang == 'de') {
      var selectCommunity = "Bitte wählen Sie eine Gemeinde aus.";
      var selectProperty = "Bitte wählen Sie ein Grundstück aus.";
      var selectBFS = "";
    } else {
      var selectCommunity = "Veuillez choisir une commune.";
      var selectProperty = "Veuillez choisir un n° bien-fonds.";
      var selectBFS = "";
    }


    // Formular-Elemente verstecken
    jQuery('.oereb-section').hide();
    jQuery('#oereb-forminput').hide();
// Beglaubigt prüfen 
jQuery('#form_form1').click(function(){
  jQuery('#oereb-adresse').removeClass("hidden");
jQuery('#oereb-kompakt-email').removeClass("hidden");

// Beglaubigt wurde ausgewählt, Submit button deaktivieren
if (jQuery('#form_form1').val() == 'beglaubigt') {
  jQuery('#form_submit').attr("disabled", "disabled");
} else {
  jQuery('#form_submit').removeAttr("disabled");
}
});

jQuery('#form-check-ok').change(function() {
var isCheckedOk = jQuery('#form-check-ok').attr('checked')?true:false;

if (isCheckedOk == true) {
  jQuery('#form_submit').removeAttr("disabled");
} else {
  jQuery('#form_submit').attr("disabled", "disabled");
}


});





      jQuery('#form-artident').change(function() {
          jQuery('.oereb-section').hide();
          jQuery('#oereb-forminput').hide();
          if (jQuery(this).val() == 'Parzellennummer') {
              jQuery('#oereb-ident-parzellennummer').show();
              jQuery('#oereb-forminput').show();
          } else {
              jQuery('#oereb-ident-parzellennummer').hide();
          }


          if (jQuery(this).val() == 'EGRID') {
              jQuery('#oereb-ident-egrid').show();
              jQuery('#oereb-forminput').show();
          } else {
              jQuery('#oereb-ident-egrid').hide();
          }
  });











// Gemeinden und Kreise
jQuery("select#form-artident").change(function() {


jQuery.getJSON(geoserviceURL + "/0/query?text=&geometry=&geometryType=esriGeometryPoint&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&objectIds=&where=OEREBSTATUS='" + oerebProtect + "'&time=&returnCountOnly=false&returnIdsOnly=false&returnGeometry=false&maxAllowableOffset=&outSR=&outFields=GEMKR_NAME,BFSNR,KREISNR&f=pjson&callback=?", function(json){


// Sorting
json.features.sort(function (a, b) {
  a = a.attributes.GEMKR_NAME,
  b = b.attributes.GEMKR_NAME;

  return a.localeCompare(b);
});

  var options='';
  var thisGemeinde='';

  options += '<option value="0" checked="checked">' + selectCommunity + '</option>';
  jQuery.each(json.features, function(i, attributes){
   jQuery.each(attributes, function(ii, gemeinde) {

      // check for duplicates
      if (thisGemeinde != gemeinde.GEMKR_NAME) {
        options += '<option value="' + gemeinde.BFSNR + ','+ gemeinde.KREISNR +'">' + gemeinde.GEMKR_NAME + '</option>';
      }
      thisGemeinde = gemeinde.GEMKR_NAME;
      kreisnr = gemeinde.KREISNR;
      gmdnr = gemeinde.BFSNR;
      });
    });

    jQuery("select#form-bfs-nummer").html(options);
  });
});


// Grundstücke



jQuery("select#form-bfs-nummer").change(function(){
var gmdekreisnr = jQuery("#form-bfs-nummer").val();
var array = gmdekreisnr.split(',');
gmdnr=array[0];
kreisnr=array[1];

jQuery.getJSON(geoserviceURL + "/1/query?text=&geometry=&geometryType=esriGeometryPoint&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&objectIds=&where=BFSNR=" + gmdnr + "+AND+KREIS=" + kreisnr +"&time=&returnCountOnly=false&returnIdsOnly=false&returnGeometry=false&maxAllowableOffset=&outSR=&outFields=NUMMER&f=pjson&callback=?", function(json) {

// Sorting
json.features.sort(function (a, b) {
  return parseInt(a.attributes.NUMMER, 10) - parseInt(b.attributes.NUMMER, 10);
});

  var options='';
  options += '<option value="0" checked="checked">'+ selectProperty +'</option>';
  jQuery.each(json.features, function(i, attributes){
   jQuery.each(attributes, function(ii, grundstueck) {

 if (grundstueck.NUMMER == 0) {
   options = '<option value="0">Kein Grundstück gefunden</option>';
 } else {
      options += '<option value="' + grundstueck.NUMMER + '">' + grundstueck.NUMMER + '</option>';
}
      });
    });

    jQuery("select#form-grundstuecknummer").html(options);
  });
});

// Grundstücknummer ist bekannt:
jQuery("select#form-grundstuecknummer").change(function(){

// Zusammenstellen der Grundstückwerte
grundstuecknummer = jQuery("select#form-grundstuecknummer").val();

// Es kann sein, dass zu einer Gemeinde keine KREISNR existiert, dieser Fall muss behandelt werden
if (typeof kreisnr == 'undefined') {
kreisnr = '';
}

// Anfügen der Werte an das Formular als Hidden-Felder
jQuery("form#ChronoContact_oerebform").append('<input type="hidden" name="parceltriple" value="' + gmdnr + ", " + kreisnr + ", " + grundstuecknummer + '" />');
// alert ("DEBUG: parceltriple = " + gmdnr + ", " + kreisnr + ", " + grundstuecknummer);

});


// Triggern des Change-Events für form-artident
jQuery('#form-artident').trigger("change");


jQuery("form").submit(function() {
// Anzeigen des modalen Hinweises
// validateUF();

var option = jQuery("#form_form1").val();

if (option!='beglaubigt' && option!='*PDFLONG') {
  // Gibt es irgendwo die Klasse errorMessage, dann nicht zeigen
  if (!jQuery('.required').hasClass('errorMessage')) {
    jQuery("#loading").addClass("show");
  }
}
});




});
