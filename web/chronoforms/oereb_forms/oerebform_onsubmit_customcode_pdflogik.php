<?php


/* ÖREB-Service URL, kann angepasst werden: */
$oerebserviceURL='https://www.oereb.apps.be.ch/OerbverSVC.svc/rest/';

/* SPRACHVARIABELN */
$PDFSHORT['DE']='Kompakter Auszug als PDF-Dokument';
$PDFSHORT['FR']='Extrait compact comme fichier PDF';
$PDFLONG['DE']='Umfassender Auszug als PDF-Dokument';
$PDFLONG['FR']='Extrait complet comme fichier PDF';
$XMLOUT['DE']='XML-Dokument';
$XMLOUT['FR']='Fichier XML';
$ERRJSON['DE']='Fehler in der JSON-Antwort (Kein JSON-Objekt)';
$ERRJSON['FR']='Erreur dans la réponse JSON (pas d\'objet JSON)';
$ERRTOKEN['DE']='Token konnte nicht korrekt gelöst werden. Bitte prüfen Sie das Passwort und den Benutzername!';
$ERRTOKEN['FR']='Une erreur s\'est produite à la création du token.';
$ERRJOBGUID['DE']='Die JOB-GUID konnte nicht korrekt gelöst werden';
$ERRJOBGUID['FR']='Une erreur s\'est produite à la création de la JOB-GUID.';
$ERRJOBPROCESS['DE']='Beim Erzeugen der Dokumente ist ein Fehler aufgetreten';
$ERRJOBPROCESS['FR']='Une Erreur s\'est produite à la création des documents';

$OUTPUTERRHEADER['DE']='Es sind Fehler aufgetreten...';
$OUTPUTERRHEADER['FR']='Des erreurs se sont produites...';
$OUTPUTERRINTRO['DE']='Leider konnte Ihre Anfrage nicht korrekt ausgeführt werden. Folgende Fehler sind bei der Bearbeitung aufgetreten:';
$OUTPUTERRINTRO['FR']='Votre demande n\'a malheureusement pas pu être effectue. Les erreurs suivantes se sont produites:';
$OUTPUTHEADER['DE']='Ihr Auszug aus dem ÖREB-Kataster ist bereit';
$OUTPUTHEADER['FR']='Votre extrait du cadastre RDPPF est disponible';
$OUTPUTHEADER_KOMPLETT['DE']='Der umfassende Auszug wird nun erstellt.';
$OUTPUTHEADER_KOMPLETT['FR']='L\'extrait complet est en cours d\'être creé';
$OUTPUTINTRO['DE']='Klicken Sie auf den untenstehenden Link um den gewünschten ÖREB-Auszug anzusehen:';
$OUTPUTINTRO['FR']='Veuillez cliquer sur le lien ci-dessous pour afficher l\'extrait:';
$OUTPUTHEADER2['DE']='Wir haben dabei folgende Angaben erhalten';
$OUTPUTHEADER2['FR']='Nous avons reçu les données suivantes';
$OUTPUTINTROBEGLAUBIGT['DE']='Wir haben Ihre Angaben erhalten und werden Ihnen den beglaubigten Auszug so rasch wie möglich per Post zukommen lassen.';
$OUTPUTINTROBEGLAUBIGT['FR']='Nous avons reçu votres données et l\'extrait certifié vous sera livré par poste dans les meilleurs delais.';

$EMAIL_KOMPLETT_BETREFF['DE']='ÖREB-Kataster-Auszug umfassend';
$EMAIL_KOMPLETT_BETREFF['FR']='Extrait complet du cadastre RDPPF';
$EMAIL_KOMPLETT_INTRO['DE']='<p>Guten Tag</p><p>Es kann einige Minuten dauern, bis der umfassende Auszug vollständig erstellt ist. Bitte haben Sie etwas Geduld. Sobald der Auszug vorliegt, kann das PDF-Dokument unter folgendem Link erreicht werden:</p>';
$EMAIL_KOMPLETT_INTRO['FR']='<p>Bonjour</p><p>La création de votre extrait complet peut prendre quelques minutes. Nous vous prions de bien vouloir patienter. Dès que l\'extrait est prêt le document peut être obtenu sous le lien suivant:</p>';
$EMAIL_KOMPLETT_FROM['DE']='ÖREB-Kataster des Kantons Bern <agi.oerebk@bve.be.ch>';
$EMAIL_KOMPLETT_FROM['FR']='Cadastre RDDPF du Canton de Berne <agi.oerebk@bve.be.ch>';
$EMAIL_KOMPLETT_SIGNATUR['DE']='<p><br>Freundliche Grüsse</p><p>ÖREB-Kataster des Kantons Bern</p><p><strong>Amt für Geoinformation des Kantons Bern</strong><br>Reiterstrasse 11, 3011 Bern, www.be.ch/agi<br>Geodaten und Karten: www.be.ch/geoportal</p>';
$EMAIL_KOMPLETT_SIGNATUR['FR']='<p><br>Meilleures salutations</p><p>Cadastre RDPPF du Canton de Berne</p><p><strong>Office pour l\'information géographique</strong><br>Reiterstrasse 11, 3011 Berne, www.be.ch/oig<br>Geodonnées et cartes: www.be.ch/geoportail</p>';
$OUTPUT_KOMPLETT['DE']='Sie haben eine Mail mit dem Link zum erstellten Auszug erhalten und Sie können diese Seite jetzt schliessen.';
$OUTPUT_KOMPLETT['FR']='Vous avez reçu un courriel avec un lien pour afficher l\'extrait. Vous pouvez fermer cette page.';

$OUTPUTBFSNR['DE']='Gemeinde-Nr';
$OUTPUTBFSNR['FR']='N° BFS de la commune';
$OUTPUTKREISNR['DE']='Grundbuchkreisnummer';
$OUTPUTKREISNR['FR']='N° de l\'arrondissement communal';
$OUTPUTGRUNDSTUECK['DE']='Grundstücknummer';
$OUTPUTGRUNDSTUECK['FR']='N° bien-fonds';
$OUTPUTEGRID['DE']='EGRID';
$OUTPUTEGRID['FR']='EGRID';
$OUTPUTFORM['DE']='Form';
$OUTPUTFORM['FR']='Type';
$OUTPUTSPRACHE['DE']='Sprache';
$OUTPUTSPRACHE['FR']='Langue';
$OUTPUTNACHNAME['DE']='Nachname (Besteller):';
$OUTPUTNACHNAME['FR']='Nom:';
$OUTPUTVORNAME['DE']='Vorname (Besteller):';
$OUTPUTVORNAME['FR']='Prénom:';
$OUTPUTADRESSE['DE']='Adresse (Besteller):';
$OUTPUTADRESSE['FR']='Adresse:';
$OUTPUTPLZORT['DE']='PLZ und Ort (Besteller):';
$OUTPUTPLZORT['FR']='NPA et lieu:';
$OUTPUTEMAIL['DE']='Email-Adresse (Besteller):';
$OUTPUTEMAIL['FR']='Courriel:';
$OUTPUTERRMISSINGWEBSERVICE['DE']='<p>Die ÖREB-Kataster-Anfrage konnte leider nicht erfolgreich durchgeführt werden.</p>
    <p>Versuchen Sie es später noch einmal, oder setzen Sie sich mit uns in Verbindung.</p>';
$OUTPUTERRMISSINGWEBSERVICE['FR']='<p>Votre demande n\'a malheureusement pas pu être effectue.</p>
    <p>Ressayez de commander un extrait un peu plus tard ou prenez contact avec nous.</p>';
$OUTPUTERRPASSWORD['DE']='Bitte prüfen Sie Ihr Passwort und Ihren Benutzernamen!';
$OUTPUTERRPASSWORD['FR']='Veuillez vérifier votre nom d\'utilisateur et votre mot de passe s.v.p.';



$oerebUSER = JRequest::getVar('oereb_user');
$oerebPW = JRequest::getVar('oereb_pw');
$oerebPROTECT = JRequest::getVar('oereb_protect');


$errorHANDLE=false;
$errorMESSAGE='';
$linebreak='<br />';

/* Language Detection */
/* Language Detection */
$doc = JFactory::getDocument();
$this->language = $doc->language;
$language = JFactory::getLanguage();
$locales = $language->getLocale();
$isolang = $language->getTag(); /* de-DE / fr-FR */
$langUpper = substr($locales[3],-2); /* DE / FR */
$langLower = substr($isolang,0,2); /* de / fr */

$lang = $langUpper;

/* Debug Language */
//echo "LANG:".$lang;
//echo "H:".$lang_code;
//$errorMESSAGE='LANGUAGE:'.$lang_code;





if ($oerebUSER == '' && $oerebPROTECT=='public'):
  $oerebUSER='ADMIN';
  $oerebPW='xVpaAhv6tY';
else:
  $OUTPUTERRMISSINGWEBSERVICE[$lang]=$OUTPUTERRPASSWORD[$lang].$linebreak.$OUTPUTERRMISSINGWEBSERVICE[$lang];
endif;


$oerebOUTPUTFORMAT= JRequest::getVar('oereb_form');

/* Nur ausführen wenn kein beglaubigter Auszug gewünscht wurde */
if ($oerebOUTPUTFORMAT !='beglaubigt'):


  // Zuerst fragen wir mal nach, ob wir ein TOKEN benutzen müssen
  $json_url = $oerebserviceURL.'info/app';
  $json = json_decode(remote_get_contents($json_url));
  if ($json==true):
    // Jetzt holen wir mal das TOKEN
    $json_url=$oerebserviceURL.'token?user='.$oerebUSER.'&pwd='.$oerebPW;
    $json_token = json_decode(remote_get_contents($json_url), true);



    /* debug */

    /*
    echo "<pre>";
    print_r($json_token);
    echo "</pre>";
    exit("FERTIG");
    */

    // Auswertung TOKEN
    $error=$json_token['Metadata']['ErrorList'][0];
    if ($error!='OK'):
      $errorHANDLE=true; // TOKEN konnte nicht erzeugt werden
      $errorMESSAGE.=$ERRTOKEN[$lang].$linebreak.'['.$error.']'.$linebreak;
    else:
      $errorHANDLE=false;
      $oerebTOKEN=$json_token['Data']['ResponseData']['Token'];
    endif;

  else:
      // Es wird kein Token benutzt... Was geschieht nun?
      // ToDo

  endif;

  // URL Zusammensetzen

  if ($errorHANDLE==false): // Kein Fehler ist aufgetreten...

  if (JRequest::getVar('oereb_artident')=='Parzellennummer'):
    $oerebKreisnummer=  JRequest::getVar('oereb_kreisnummer');
    if ($oerebKreisnummer=='' or empty($oerebKreisnummer)):
      $oerebKreisnummer=0;
    endif;

    // Die Parzellennummer wird als parceltriple angefügt
    $oerebPARAM = 'parceltriple='.JRequest::getVar('oereb_bfs-nummer').",".JRequest::getVar('oereb_grundstuecknummer');
  else:
    // Es wird nur der Parameter egrid mitgegeben
    $oerebPARAM = 'egrid='.JRequest::getVar('egrid');
  endif;

  $async = JRequest::getVar('async');
  /* immmer auf true setzen... */
  $async='true';

  // Wenn ein umfassender Auszug bestellt wurde, werden ein paar Texte ausgetauscht...
  if (stristr($oerebOUTPUTFORMAT,'PDFLONG')):
    $OUTPUTINTRO[$lang]=$OUTPUT_KOMPLETT[$lang];
    $OUTPUTHEADER[$lang]=$OUTPUTHEADER_KOMPLETT[$lang];
  endif;


  $oerebPARAM.='&language='.JRequest::getVar('oereblang');
  $oerebPARAM.='&oevthemes='.JRequest::getVar('oevthemes');
  $oerebPARAM.='&asynch='.$async;
  $oerebPARAM.='&token='.$oerebTOKEN;
  $oerebPARAM.='&outputformat='.$oerebOUTPUTFORMAT;
  $oerebPARAM.='&outputdeliver=*URI';


  /* JOB holen: auswerten und auf Fehler prüfen */
  $json_url = $oerebserviceURL.'extract?'.$oerebPARAM;

  /* debug */
  //echo '<p>URL: '.$json_url.'</p>';
  //exit("FERTIG");

  $json = json_decode(remote_get_contents($json_url), true);
  if ($json==true):

      /*
       echo "<pre>";
      print_r($json);
      echo "</pre>";
      */

    // Auswertung JOB-GUID
    $error=$json['Metadata']['ErrorList'][0];
    if ($error!='OK'):
      $errorHANDLE=true; // Ein Fehler ist aufgetreten
      $errorMESSAGE.=$ERRJOBGUID[$lang].$linebreak;
    else:

      $errorHANDLE=false;
      $oerebJOBGUID=$json['Data']['ResponseData']['JobData']['Job'];

      $oerebEXTRACT=$json;

      /* Auslesen der URIs */
      $downloadURI='';
      if (stristr($oerebOUTPUTFORMAT,'PDFSHORT')):
        $downloadURI.='<p><a class="icon pdf" target="_blank" href="'.$oerebEXTRACT['Data']['ResponseData']['JobData']['PdfDocumentShortUri'].'">'.$PDFSHORT[$lang].'</a></p>';
      endif;

      if (stristr($oerebOUTPUTFORMAT,'PDFLONG')):
        $downloadURI.='<p><a class="icon pdf" target="_blank" href="'.$oerebEXTRACT['Data']['ResponseData']['JobData']['PdfDocumentLongUri'].'">'.$PDFLONG[$lang].'</a></p>';
      endif;

      if (stristr($oerebOUTPUTFORMAT,'XML')):
        $downloadURI.='<p><a class="icon file" target="_blank"  href="'.$oerebEXTRACT['Data']['ResponseData']['JobData']['XmlDocumentUri'].'">'.$XMLOUT[$lang].'</a></p>';
      endif;


      /*
      echo "<h2>Resultat der Methode 'extract'</h2>";
      echo "<pre>";
      print_r($oerebEXTRACT);
      echo "</pre>";
      */
      endif;

  else:
    $errorMESSAGE.='ERR_JSON'.$linebreak; // JSON-FEHLER
  endif;

  /* JOB-KONTROLLIEREN */
  if ($errorHANDLE==false): /* Kein Fehler ist aufgetreten */
    /* POLLING STATUS */
    $oerebPARAM='job='.$oerebJOBGUID;
    $json_url = $oerebserviceURL.'/job?'.$oerebPARAM;
    /* debug */
    // echo '<p>URL: '.$json_url.'</p>';

    $i=0;

    /* Hier wird das Mail verschickt für den grossen Auszug */
    if (stristr($oerebOUTPUTFORMAT,'PDFLONG')):

    // Empfänger
      $empfaenger  = JRequest::getVar('oereb_email_komplett');
       // Betreff
      $betreff = $EMAIL_KOMPLETT_BETREFF[$lang];

        // Nachricht
      $nachricht = '
      <html>
      <head>
        <title>'.$EMAIL_KOMPLETT_BETREFF[$lang].'</title>
      </head>
      <body style="font-family:Arial,sans-serif;">
        <p>'.$EMAIL_KOMPLETT_INTRO[$lang].'</p>
        <p>'.$downloadURI.'</p>
        <p><strong>'.$OUTPUTHEADER2[$lang].'</strong></p>
        <table class="alternative">
        ';

        if (JRequest::getVar('oereb_artident')=='Parzellennummer') {
          $nachricht.='
          <tr>
          <td>'.$OUTPUTBFSNR[$lang].', '.$OUTPUTKREISNR[$lang].'</td>
          <td>'.JRequest::getVar('oereb_bfs-nummer').'</td>
          </tr>

          ';
          $nachricht.='
          <tr>
          <td>'.$OUTPUTGRUNDSTUECK[$lang].'</td>
          <td>'.JRequest::getVar('oereb_grundstuecknummer').'</td>
          </tr>';
        } else {  // ELSE ARTIDENT = EGRID
          $nachricht.='
          <tr>
          <td>'.$OUTPUTEGRID[$lang].'</td>
          <td>'.JRequest::getVar('egrid').'</td>
          </tr>
          ';
        } // ENDE ARTIDENT = EGRID
        $nachricht.='
        <tr>
        <td>'.$OUTPUTFORM[$lang].'</td>
        <td>'.JRequest::getVar('oereb_form').'</td>
        </tr>
        <tr>
        <td>'.$OUTPUTSPRACHE[$lang].'</td>
        <td>'.JRequest::getVar('oereblang').'</td>
        </tr></table>'.$EMAIL_KOMPLETT_SIGNATUR[$lang].'



      </body>
      </html>
      ';

      $mail = &JFactory::getMailer();
      $mail->addRecipient($empfaenger);
      $mail->setSender('agi.oerebk@bve.be.ch');
      $mail->setSubject($betreff);
      $mail->setBody($nachricht);
      $mail->IsHTML(true);
      $mail->Encoding = 'base64';
      $mail->Send();


    endif;


    ob_implicit_flush(true);
    echo str_repeat(' ',1024*32);
    flush();







    /* ENDE MAIL */
   if ($empfaenger==''):
     do {
        usleep(100000); /* Zuerst warten wir mal 0.1s (100000 u-sekunden) */
        $json = json_decode(remote_get_contents($json_url), true);
        if ($json==true):
          /*
           echo "<pre>";
          print_r($json);
          echo "</pre>";
          */

          $error=$json['Metadata']['ErrorList'][0];


          if ($error!='OK'):
            $errorHANDLE=true; // Ein Fehler ist aufgetreten
            $errorMESSAGE.=$ERRJOBPROCESS[$lang].' ('.$error.' ['.$oerebSTATUS.'])'.$linebreak;
            break;
          else:
            $errorHANDLE=false;
            $oerebSTATUS=$json['Data']['ResponseData']['ProcessingStatus'];

          endif;

          /* Flushing output buffer */
          if ($i % 2):
            echo str_repeat(' ',1024*32);
          endif;

          echo str_repeat(' ',1024*32);




          $i=$i+1;

          // Javascript for updating the progress bar and information

          echo " ";



        else:
          $errorMESSAGE=$ERRJSON[$lang].$linebreak; /* Im Job-process ist ein Fehler aufgetreten */
          /* ToDo: detaillierte Fehlerausgabe */
          $errorHANDLE=true;
          break;

        endif; /* JSON==True */
      } while ($oerebSTATUS!='FinishedOK');
    endif;
    /* ENDE POLLING STATUS */


        /*
        echo "<pre>";
        print_r($json);
        echo "</pre>";
        */

  endif;
  /* ENDE JOB-KONTROLLIEREN */




    if ($errorMESSAGE!=''):    // Fehler sind aufgetreten, Ausgabe der Fehlermeldung
      $output='<h1>'.$OUTPUTERRHEADER[$lang].'</h1>
      <p>'.$OUTPUTERRINTRO[$lang].'</p>';
      $output.='<p><strong>'.$errorMESSAGE.'</strong></p>';

    else: // Keine Fehler sind aufgetreten...


    $output='
    <h1>'.$OUTPUTHEADER[$lang].'</h1>
    <p>'.$OUTPUTINTRO[$lang].'</p>';

    if ($oerebOUTPUTFORMAT!='*PDFLONG'):
      $output.=$downloadURI;
    endif;

    $output.='
    <h2>'.$OUTPUTHEADER2[$lang].'</h2>
    <table class="alternative">
    ';

    if (JRequest::getVar('oereb_artident')=='Parzellennummer') {
      $output.='
      <tr>
      <th>'.$OUTPUTBFSNR[$lang].', '.$OUTPUTKREISNR[$lang].'</th>
      <td>'.$form->data['oereb_bfs-nummer'].'</td>
      </tr>

      ';
      $output.='
      <tr>
      <th>'.$OUTPUTGRUNDSTUECK[$lang].'</th>
      <td>'.$form->data['oereb_grundstuecknummer'].'</td>
      </tr>';
    } else {  // ELSE ARTIDENT = EGRID
      $output.='
      <tr>
      <th>'.$OUTPUTEGRID[$lang].'</th>
      <td>'.$form->data['egrid'].'</td>
      </tr>
      ';
    } // ENDE ARTIDENT = EGRID
    $output.='
    <tr>
    <th>'.$OUTPUTFORM[$lang].'</th>
    <td>'.$form->data['oereb_form'].'</td>
    </tr>
    <tr>
    <th>'.$OUTPUTSPRACHE[$lang].'</th>
    <td>'.$form->data['oereblang'].'</td>
    </tr>
    ';

    /* Email-Adresse für den langen Auszug anzeigen */
    if (stristr($oerebOUTPUTFORMAT,'PDFLONG')):
      $output.='
        <tr>
        <th>'.$OUTPUTEMAIL[$lang].'</th>
        <td>'.$form->data['oereb_email_komplett'].'</td>
        </tr>
      ';
    endif;

    if (stristr($oerebOUTPUTFORMAT,'beglaubigt')):
      $output.='
        <tr>
        <th>'.$OUTPUTNACHNAME[$lang].'</th>
        <td>'.$form->data['oereb_nachname'].'</td>
        </tr>
        <tr>
        <th>'.$OUTPUTVORNAME[$lang].'</th>
        <td>'.$form->data['oereb_vorname'].'</td>
        </tr>
        <tr>
        <th>'.$OUTPUTADRESSE[$lang].'</th>
        <td>'.$form->data['oereb_adresse'].'</td>
        </tr>
        <tr>
        <th>'.$OUTPUTPLZORT[$lang].'</th>
        <td>'.$form->data['oereb_plz'].'&nbsp;'.$form->data['oereb_ort'].'</td>
        </tr>
        <tr>
        <th>'.$OUTPUTEMAIL[$lang].'</th>
        <td>'.$form->data['oereb_email'].'</td>
        </tr>
      ';
    endif; // end beglaubigt

    $output.="</table>";

    endif; // Ende Fehlerprüfung aufgetreten


  /* Fehlerausgabe */
  else: // errorHANDLE=true
     $output='<h2>'.$OUTPUTERRHEADER[$lang].'</h2>'.$OUTPUTERRMISSINGWEBSERVICE[$lang];
  endif; /* Ende Fehlerausgabe */


  echo $output;

/* Nur ausführen wenn kein beglaubigter Auszug gewünscht wurde */
else:
  $output='
  <h1>'.$OUTPUTHEADER[$lang].'</h1>
  <p>'.$OUTPUTINTROBEGLAUBIGT[$lang].'</p>
  <h2>'.$OUTPUTHEADER2[$lang].'</h2>
  <table class="alternative">
  ';

  if (JRequest::getVar('oereb_artident')=='Parzellennummer') {
    $output.='
    <tr>
    <th>'.$OUTPUTBFSNR[$lang].', '.$OUTPUTKREISNR[$lang].'</th>
    <td>'.$form->data['oereb_bfs-nummer'].'</td>
    </tr>

    ';
    $output.='
    <tr>
    <th>'.$OUTPUTGRUNDSTUECK[$lang].'</th>
    <td>'.$form->data['oereb_grundstuecknummer'].'</td>
    </tr>';
  } else {  // ELSE ARTIDENT = EGRID
    $output.='
    <tr>
    <th>'.$OUTPUTEGRID[$lang].'</th>
    <td>'.$form->data['egrid'].'</td>
    </tr>
    ';
  } // ENDE ARTIDENT = EGRID
  $output.='
  <tr>
  <th>'.$OUTPUTFORM[$lang].'</th>
  <td>'.$form->data['oereb_form'].'</td>
  </tr>
  <tr>
  <th>'.$OUTPUTSPRACHE[$lang].'</th>
  <td>'.$form->data['language'].'</td>
  </tr>
  ';

  if (stristr($oerebOUTPUTFORMAT,'beglaubigt')):
    $output.='
      <tr>
      <th>'.$OUTPUTNACHNAME[$lang].'</th>
      <td>'.$form->data['oereb_nachname'].'</td>
      </tr>
      <tr>
      <th>'.$OUTPUTVORNAME[$lang].'</th>
      <td>'.$form->data['oereb_vorname'].'</td>
      </tr>
      <tr>
      <th>'.$OUTPUTADRESSE[$lang].'</th>
      <td>'.$form->data['oereb_adresse'].'</td>
      </tr>
      <tr>
      <th>'.$OUTPUTPLZORT[$lang].'</th>
      <td>'.$form->data['oereb_plz'].'&nbsp;'.$form->data['oereb_ort'].'</td>
      </tr>
      <tr>
      <th>'.$OUTPUTEMAIL[$lang].'</th>
      <td>'.$form->data['oereb_email'].'</td>
      </tr>
    ';
  endif; // end beglaubigt

  $output.="</table>";

  echo $output;

endif;
/* Nur ausführen wenn  nicht kein beglaubigter Auszug gewünscht wurde */


/*****************************************************************
             Functions
*****************************************************************/



// Get remote file contents, preferring faster cURL if available
function remote_get_contents($url)
{
        if (function_exists('curl_get_contents') AND function_exists('curl_init'))
        {
                return curl_get_contents($url);
        }
        else
        {
                // A litte slower, but (usually) gets the job done
                return file_get_contents($url);
        }
}

function curl_get_contents($url)
{
        // Initiate the curl session
        $ch = curl_init();

        // Set the URL
        curl_setopt($ch, CURLOPT_URL, $url);

        // Without certificates
        // REMOVE BEFORE FLIGHT
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // Removes the headers from the output
        curl_setopt($ch, CURLOPT_HEADER, 0);

        // Return the output instead of displaying it directly
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // Execute the curl session
        $output = curl_exec($ch);

        // Close the curl session
        curl_close($ch);

        // Return the output as a variable
        return $output;
}
?>
