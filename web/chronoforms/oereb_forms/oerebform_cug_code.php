<?php
  /* Zuerst wird auf verfügbarkeit des Dienstes geprüft */
  /* Variabeln */
  /* ÖREB-Service URL, kann angepasst werden: */
  $oerebserviceURL='https://www.oereb.apps.be.ch/OerbverSVC.svc/rest/';

  // Diese Variable auf 'false' setzen, falls es sich um das öffentliche Formular
  // handelt. Andernfalls wird Passwort und Benutzername abgefragt.
  $oerebProtect = true;

    /* Sprachvariabeln */
  $TXT_PFLICHTFELD['DE']="Pflichtfeld";
  $TXT_HINWEISNOETIGESFELD['DE']=", benötigen wir für eine Kontaktaufnahme.";
  $TXT_LABEL_FORM_ARTIDENT['DE']="Grundstücksidentifikation über";
  $TXT_TITLE_FORM_ARTIDENT['DE']="Bitte wählen Sie die Art der Grundstücksidentifikation aus.";
  $TXT_OPTION_SELECT_DEFAULT['DE']="Bitte wählen...";
  $TXT_OPTION_PARZELLENNUMMER['DE']="Parzellennummer";
  $TXT_OPTION_EGRID['DE']="EGRID";
  $TXT_LABEL_FORM_BFS_NUMMER['DE']="Gemeinde";
  $TXT_TITLE_FORM_BFS_NUMMER['DE']="Bitte wählen Sie eine Gemeinde aus.";
  $TXT_DATENWERDENGELADEN['DE']="Daten werden geladen...";
  $TXT_LABEL_FORM_KREISNUMMER['DE']="Grundbuchkreis";
  $TXT_TITLE_FORM_KREISNUMMER['DE']="Bitte wählen Sie eine Kreisnummer aus.";
  $TXT_LABEL_FORM_GRUNDSTUECKSNUMMER['DE']="Grundstücksnummer";
  $TXT_TITLE_FORM_GRUNDSTUECKSNUMMER['DE']="Bitte wählen Sie ein Grundstück aus.";
  $TXT_LABEL_FORM_EGRID['DE']="EGRID";
  $TXT_TITLE_FORM_EGRID['DE']="";
  $TXT_LABEL_OEREB_TYP['DE']="Ausprägung";
  $TXT_INPUT_OEREB_TYP_REDUZIERT['DE']="Reduziert";
  $TXT_INPUT_OEREB_TYP_BEGLAUBIGT['DE']="Beglaubigter Auszug (kostenpflichtig)";
  $TXT_LABEL_OUTPUTFORMAT['DE']="&nbsp;";
  $TXT_INPUT_OUTPUTFORMAT_PDFSHORT['DE']="Kompakter Auszug";
  $TXT_INPUT_OUTPUTFORMAT_PDFLONG['DE']="Umfassender Auszug";
  $TXT_INPUT_OUTPUTFORMAT_XML['DE']="XML";
  $TXT_LABEL_FORM_SPRACHE['DE']="Sprache";
  $TXT_INPUT_FORM_SPRACHE1['DE']="Deutsch";
  $TXT_INPUT_FORM_SPRACHE2['DE']="Französisch";
  $TXT_LABEL_FORM_NACHNAME['DE']="Nachname Besteller";
  $TXT_TITLE_FORM_NACHNAME['DE']="Bitte geben Sie Ihren Nachnamen an.";
  $TXT_LABEL_FORM_VORNAME['DE']="Vorname Besteller";
  $TXT_TITLE_FORM_VORNAME['DE']="Bitte geben Sie Ihren Vornamen an.";
  $TXT_LABEL_FORM_ADRESSE['DE']="Adresse Besteller";
  $TXT_TITLE_FORM_ADRESSE['DE']="Bitte geben Sie Ihre Adresse an.";
  $TXT_LABEL_FORM_PLZ['DE']="PLZ Besteller";
  $TXT_TITLE_FORM_PLZ['DE']="Bitte geben Sie eine Postleitzahl an.";
  $TXT_LABEL_FORM_ORT['DE']="Ort Besteller";
  $TXT_TITLE_FORM_ORT['DE']="Bitte geben Sie Ihren Wohnort an.";
  $TXT_LABEL_FORM_EMAIL['DE']="E-Mail";
  $TXT_LABEL_FORM_EMAIL_HINT['DE']="Bitte tragen Sie Ihre E-Mail-Adresse ein. Sie werden per Mail benachrichtigt, so bald der Auszug für Sie bereitliegt.";
  $TXT_TITLE_FORM_EMAIL['DE']="Bitte geben Sie eine gültige E-Mail-Adresse ein!";
  $TXT_OEREB_SERVICE_DOWN['DE']='
  <h2>Die ÖREB-Kataster-Abfrage kann zur Zeit nicht benutzt werden</h2>
  <h3>Leider ist der Dienst im Augenblick nicht verfügbar. Bitte versuchen Sie es zu einem späteren Zeitpunkt noch einmal.</h3>
  <h3>Wir entschuldigen uns für die Unannehmlichkeiten</h3>';
  $TXT_OEREB_LOADING['DE']='
  <h2>Die Anfrage wurde gestartet...</h2>
  <p>Der Auszug wird nun vorbereitet. Dieser Vorgang kann ein paar Sekunden dauern. Bitte haben Sie etwas Geduld.</p>';
  $TXT_OEREB_SUBMIT_BEGLAUBIGT['DE']='
  <h2>Sie haben einen beglaubigten Auszug bestellt</h2>
  <p>Wir machen Sie darauf Aufmerksam, dass die Bestellung eines beglaubigten Auszuges kostenpflichtig ist.</p>';
  $TXT_LABEL_FORM_CHECK_OK['DE']="Ich bin mit den Kosten einverstanden.";
  $TXT_HINT_FORM_CHECK_OK['DE']="Durch das Absenden der obigen Anfrage für einen beglaubigten Auszug entstehen <strong>Kosten von CHF 60.-</strong>. Die Rechnung wird an die angegebene Adresse gesendet.";
  $TXT_LABEL_FORM_USER['DE']="Benutzername";
  $TXT_LABEL_FORM_PW['DE']='Passwort';
  $TXT_TITEL_FORM_USER['DE']="Bitte tragen Sie Ihren Benutzername ein.";
  $TXT_TITEL_FORM_PW['DE']='Bitten geben Sie das entsprechende Passwort zu Ihrem Benutzername ein.';
$TXT_HINT_FORM_SUBMIT['DE']='Absenden';

  /* FRANZÖSISCH */
   $TXT_PFLICHTFELD['FR']="Champ obligatoire";
  $TXT_HINWEISNOETIGESFELD['FR']=", benötigen wir für eine Kontaktaufnahme.";
  $TXT_LABEL_FORM_ARTIDENT['FR']="Mode d'identification du bien-fonds";
  $TXT_TITLE_FORM_ARTIDENT['FR']="Veuillez choisir le mode d'identification du bien-fonds.";
  $TXT_OPTION_SELECT_DEFAULT['FR']="Veuillez choisir...";
  $TXT_OPTION_PARZELLENNUMMER['FR']="N° bien-fonds";
  $TXT_OPTION_EGRID['FR']="EGRID";
  $TXT_LABEL_FORM_BFS_NUMMER['FR']="Commune";
  $TXT_TITLE_FORM_BFS_NUMMER['FR']="Veuillez choisir une commune...";
  $TXT_DATENWERDENGELADEN['FR']="Les données sont chargées...";
  $TXT_LABEL_FORM_KREISNUMMER['FR']="Arrondissement";
  $TXT_TITLE_FORM_KREISNUMMER['FR']="Veuillez choisir un arrondissement.";
  $TXT_LABEL_FORM_GRUNDSTUECKSNUMMER['FR']="N° bien-fonds";
  $TXT_TITLE_FORM_GRUNDSTUECKSNUMMER['FR']="Veuillez choisir un n° bien-fonds.";
  $TXT_LABEL_FORM_EGRID['FR']="EGRID";
  $TXT_TITLE_FORM_EGRID['FR']="EGRID";
  $TXT_LABEL_OEREB_TYP['FR']="Type";
  $TXT_INPUT_OEREB_TYP_REDUZIERT['FR']="Extrait compact";
  $TXT_INPUT_OEREB_TYP_BEGLAUBIGT['FR']="Extrait certifié (payant)";
  $TXT_LABEL_OUTPUTFORMAT['FR']="&nbsp;";
  $TXT_INPUT_OUTPUTFORMAT_PDFSHORT['FR']="Extrait compact";
  $TXT_INPUT_OUTPUTFORMAT_PDFLONG['FR']="Extrait complet";
  $TXT_INPUT_OUTPUTFORMAT_XML['FR']="XML";
  $TXT_LABEL_FORM_SPRACHE['FR']="Langue";
  $TXT_INPUT_FORM_SPRACHE1['FR']="Allemand";
  $TXT_INPUT_FORM_SPRACHE2['FR']="Français";
  $TXT_LABEL_FORM_NACHNAME['FR']="Nom";
  $TXT_TITLE_FORM_NACHNAME['FR']="Veuillez indiquer votre nom.";
  $TXT_LABEL_FORM_VORNAME['FR']="Prénom";
  $TXT_TITLE_FORM_VORNAME['FR']="Veuillez indiquer votre prénom.";
  $TXT_LABEL_FORM_ADRESSE['FR']="Adresse";
  $TXT_TITLE_FORM_ADRESSE['FR']="Veuillez indiquer votre adresse.";
  $TXT_LABEL_FORM_PLZ['FR']="NPA";
  $TXT_TITLE_FORM_PLZ['FR']="Veuillez indiquer un NPA.";
  $TXT_LABEL_FORM_ORT['FR']="Lieu";
  $TXT_TITLE_FORM_ORT['FR']="Veuillez indiquer votre lieu.";
  $TXT_LABEL_FORM_EMAIL['FR']="Courriel";
  $TXT_LABEL_FORM_EMAIL_HINT['FR']="Veuillez indiquer votre courriel..";
  $TXT_TITLE_FORM_EMAIL['FR']="Veuillez indiquer votre courriel!";
  $TXT_OEREB_SERVICE_DOWN['FR']='
  <h2>Le système ne peut pas être utilisé en ce moment</h2>
  <h3>Malheureusement le service n\'est pas disponible. Ressayez encore une fois un peu plus tard.</h3>
  <h3>Nous vous présentons nos excuses pour les ennuis que vous a causés cette erreur.</h3>';
  $TXT_OEREB_LOADING['FR']='
  <h2>La demande est lancée...</h2>
  <p>L\'extrait est en train d\'être etabli. Cette opération peut prendre quelques secondes. Veuillez patienter un instant.</p>';
  $TXT_OEREB_SUBMIT_BEGLAUBIGT['FR']='
  <h2>Sie haben einen beglaubigten Auszug bestellt</h2>
  <p>Wir machen Sie darauf Aufmerksam, dass die Bestellung eines beglaubigten Auszuges kostenpflichtig ist.</p>';
  $TXT_LABEL_FORM_CHECK_OK['FR']="Je suis d'accord avec les frais.";
  $TXT_HINT_FORM_CHECK_OK['FR']="La demande d'un extrait certifié entraîne des <strong>frais de CHF 60.-</strong>. La facture sera envoyée à l'adresse indiquée.";
  $TXT_LABEL_FORM_USER['FR']="Utilisateur";
  $TXT_LABEL_FORM_PW['FR']='Mot de passe';
  $TXT_TITEL_FORM_USER['FR']="Veuillez indiquer votre nom d'utilisateur";
  $TXT_TITEL_FORM_PW['FR']='Veuillez indiquer votre mot de passe.';
$TXT_HINT_FORM_SUBMIT['FR']='Envoyer';



  /* Ab hier nichts mehr verändern */
  $webservice=false;

  /* Test ob der Web-Service läuft */
  $json_url = $oerebserviceURL.'isalive';
  $json = json_decode(remote_get_contents($json_url));



  if (json_last_error() === JSON_ERROR_NONE) {
    if ($json==true):
      $webservice=true;
    endif;
  }



    /* Language Detection */
    $doc = JFactory::getDocument();
    $this->language = $doc->language;
    $language = JFactory::getLanguage();
    $locales = $language->getLocale();
    $isolang = $language->getTag(); /* de-DE / fr-FR */
    $langUpper = substr($locales[3],-2); /* DE / FR */
    $langLower = substr($isolang,0,2); /* de / fr */

    $lang = $langUpper;


//  echo "LANG:".$lang;
// echo "H:".$lang_code;

  /* Validation Scripts */

  $doc->addScript('templates/geoportal14/js/usable_forms.js');
  $doc->addScript('templates/geoportal14/js/validation.js');


  if ($webservice==true):
    /* Dienst läuft und wir können das Formular einmal anzeigen */
  ?>
  <p>* <?php echo $TXT_PFLICHTFELD[$lang];?></p>

  <?php
  // Hier wird User / Passwort-Feld eingeblendet
  if ($oerebProtect==true):
  ?>
  <div class="row">
  <label for="form-user"><?php echo $TXT_LABEL_FORM_USER[$lang];?></label>
      <input class="text large required" title="<?php echo $TXT_TITEL_FORM_USER[$lang];?>" id="form-user" name="oereb_user" type="text" />
  </div>

  <div class="row">
  <label for="form-passwort"><?php echo $TXT_LABEL_FORM_PW[$lang];?></label>
      <input class="text large required" title="<?php echo $TXT_TITEL_FORM_PW[$lang];?>" id="form-pw" name="oereb_pw" type="password" />
  </div>
  <?php
  endif;
  // Ende User / Passwort-Feld
  ?>

  <div class="row">
  <label for="form-artident"><?php echo $TXT_LABEL_FORM_ARTIDENT[$lang];?></label>
  <select class="full required" id="form-artident" title="<?php echo $TXT_TITLE_FORM_ARTIDENT[$lang];?>"  name="oereb_artident">

  <option selected="selected" value="Parzellennummer"><?php echo $TXT_OPTION_PARZELLENNUMMER[$lang];?></option>
  <option value="EGRID"><?php echo $TXT_OPTION_EGRID[$lang];?></option>

      </select>
  </div>
  <!-- START PARZELLENNUMMER -->
  <div class="oereb-section" id="oereb-ident-parzellennummer">

  <div class="row">
  <label for="form-bfs-nummer"><?php echo $TXT_LABEL_FORM_BFS_NUMMER[$lang];?></label>
  <select class="full required" id="form-bfs-nummer" title="<?php echo $TXT_TITLE_FORM_BFS_NUMMER[$lang];?>"  name="oereb_bfs-nummer">

    <option selected="selected" value=""><?php echo $TXT_DATENWERDENGELADEN[$lang];?></option>

  </select>
  </div>

  <!--
  <div class="row">
  <label for="form-kreisnummer"><?php echo $TXT_LABEL_FORM_KREISNUMMER[$lang];?></label>
  <select class="full required" id="form-kreisnummer" title="<?php echo $TXT_TITLE_FORM_KREISNUMMER[$lang];?>"  name="oereb_kreisnummer">

    <option select="selected" value=""><?php echo $TXT_DATENWERDENGELADEN[$lang];?></option>

  </select>
  </div>
   -->
  <input type="hidden" id="form-kreisnummer" name="oereb_kreisnummer" value="" />

  <div class="row">
  <label for="form-grundstuecknummer"><?php echo $TXT_LABEL_FORM_GRUNDSTUECKSNUMMER[$lang];?></label>
  <select class="full required" id="form-grundstuecknummer" title="<?php echo $TXT_TITLE_FORM_GRUNDSTUECKSNUMMER[$lang];?>"  name="oereb_grundstuecknummer">

    <option selected="selected" value=""><?php echo $TXT_DATENWERDENGELADEN[$lang];?></option>

  </select>
  </div>

  </div>
  <!-- ENDE PARZELLENNUMMER -->



  <!-- START EGRID -->
  <div class="oereb-section" id="oereb-ident-egrid">

  <div class="row">
  <label for="form-egrid"><?php echo $TXT_LABEL_FORM_EGRID[$lang];?></label>
      <input class="text large" title="" id="form-egrid" name="egrid" type="text" />
  </div>

  </div>
  <!-- ENDE EGRID -->

  <!-- START NORMAL OEREB -->
  <div class="oereb-section" id="oereb-forminput">

  <!-- HIER FORM -->

  <div class="row">
  <label for="oereb_form"><?php echo $TXT_LABEL_OEREB_TYP[$lang];?></label>
  <select class="full" id="form_form1" name="oereb_form">
        <option selected="selected" value="*PDFSHORT" title="" rel="none"><?php echo $TXT_INPUT_OUTPUTFORMAT_PDFSHORT[$lang]; ?></option>
        <option value="*PDFLONG" rel="emailValidate"><?php echo $TXT_INPUT_OUTPUTFORMAT_PDFLONG[$lang];?></option>
        <!--<option value="*XML" rel="none"><?php echo $TXT_INPUT_OUTPUTFORMAT_XML[$lang];?></option>-->

    </select>
  </div>
  <div class="form-item hidden" id="oereb-kompakt-email" rel="emailValidate">
	  <div class="row">
	      <label for="form-email-komplett"><?php echo $TXT_LABEL_FORM_EMAIL[$lang];?>*&nbsp;<span class="hidden"><?php echo $TXT_HINWEISNOETIGESFELD[$lang];?></span></label>
	      <input class="text large required" validation="required" title="<?php echo $TXT_TITLE_FORM_EMAIL[$lang];?>" id="form-email-komplett" name="oereb_email_komplett" type="text" />
	      <br />
	      <span class="tipp"><?php echo $TXT_LABEL_FORM_EMAIL_HINT[$lang];?></span>
	  </div>
	</div>

  <!-- ENDE FORM -->

  <!-- HIER SPRACHE -->
  <div class="row">
  <label for="form-sprache"><?php echo $TXT_LABEL_FORM_SPRACHE[$lang];?></label>
  <?php
  $langSelected['DE'] = '';
  $langSelected['FR'] = '';
  $langSelected[$lang] = 'selected="selected"';
   ?>
  <select class="full" name="oereblang" id="oereb-sprache">
        <option value="DE" <?php echo $langSelected['DE'];?>><?php echo $TXT_INPUT_FORM_SPRACHE1[$lang];?></option>
        <option value="FR" <?php echo $langSelected['FR'];?>><?php echo $TXT_INPUT_FORM_SPRACHE2[$lang];?></option>
  </select>
  </div>
  <!-- ENDE SPRACHE -->

  <!-- START ADRESSE -->


  <div class="form-item hidden" id="oereb-adresse" rel="adresseValidate">

  <div class="row">
  <label for="form-nachname"><?php echo $TXT_LABEL_FORM_NACHNAME[$lang];?>*&nbsp;<span class="hidden"><?php echo $TXT_HINWEISNOETIGESFELD[$lang];?></span></label>
      <input class="text large required" title="<?php echo $TXT_TITLE_FORM_NACHNAME[$lang];?>" validation="required" id="form-nachname" name="oereb_nachname" type="text" />
  </div>

  <div class="row">
  <label for="form-vorname"><?php echo $TXT_LABEL_FORM_VORNAME[$lang];?>*&nbsp;<span class="hidden"><?php echo $TXT_HINWEISNOETIGESFELD[$lang];?></span></label>
      <input class="text large required" title="<?php echo $TXT_TITLE_FORM_VORNAME[$lang];?>" validation="required" id="form-vorname" name="oereb_vorname" type="text" />
  </div>

  <div class="row">
      <label for="form-adresse"><?php echo $TXT_LABEL_FORM_ADRESSE[$lang];?>*&nbsp;<span class="hidden"><?php echo $TXT_HINWEISNOETIGESFELD[$lang];?></span></label>
      <textarea class="text full required" cols="30" rows="5" validation="required" id="form-adresse" title="<?php echo $TXT_TITLE_FORM_ADRESSE[$lang];?>" name="oereb_adresse"></textarea>
  </div>

  <div class="row">
  <label for="form-plz"><?php echo $TXT_LABEL_FORM_PLZ[$lang]; ?>*&nbsp;<span class="hidden"><?php echo $TXT_HINWEISNOETIGESFELD[$lang];?></span></label>
      <input class="text small required" title="<?php echo $TXT_TITLE_FORM_PLZ[$lang];?>" validation="required" id="form-plz" name="oereb_plz" type="text" />
  </div>

  <div class="row">
  <label for="form-ort"><?php echo $TXT_LABEL_FORM_ORT[$lang];?>*&nbsp;<span class="hidden"><?php echo $TXT_HINWEISNOETIGESFELD[$lang];?></span></label>
      <input class="text large required" title="<?php echo $TXT_TITLE_FORM_ORT[$lang];?>" validation="required" id="form-ort" name="oereb_ort" type="text" />
  </div>

  <div class="row">
      <label for="form-email"><?php echo $TXT_LABEL_FORM_EMAIL[$lang];?>*&nbsp;<span class="hidden"><?php echo $TXT_HINWEISNOETIGESFELD[$lang];?></span></label>
      <input class="text large required" title="<?php echo $TXT_TITLE_FORM_EMAIL[$lang];?>" validation="required" id="form-email" name="oereb_email" type="text" />
  </div>
  <div class="row no-label">
   	<input value="ok" class="checkbox required" id="form-check-ok" name="check-ok[]" type="checkbox" />
    <label for="form-check-ok" class="checkbox"><?php echo $TXT_LABEL_FORM_CHECK_OK[$lang];?>*&nbsp;<span class="hidden"><?php echo $TXT_HINWEISNOETIGESFELD[$lang];?></span></label>
  	<br />
  	<span class="tipp"><?php echo $TXT_HINT_FORM_CHECK_OK[$lang];?></span>
  </div>




  <!-- ENDE ADRESSE -->


  </div>
  <!-- ENDE NORMAL OEREB -->

  <div class="row">
      <input id="form_submit" class="submit" value="<?php echo $TXT_HINT_FORM_SUBMIT[$lang];?>" name="submit" type="submit" />
  </div>



  <?php
  // Im Falle eines geschützten Aufrufes via Bentuername / Passwort, wird der Parameter oereb_protect auf 'cug' gesetzt, sonst ist er 'public'
  if ($oerebProtect==true):
  ?>
  <input value="cug" id="hidden_oerebprotect" name="oereb_protect" type="hidden" />
  <?php
  else:
  ?>
  <input value="public" id="hidden_oerebprotect" name="oereb_protect" type="hidden" />
  <?php
  endif;
  ?>
  <input value="*ALL" id="hidden_outputdeliver" name="outputdeliver" type="hidden" />
  <input value="true" id="hidden_async" name="async" type="hidden" />
  <input value="*ALL" id="hidden_oevthemes" name="oevthemes" type="hidden" />
  <input value="<?php echo $lang;?>" id="lang" name="lang" type="hidden" />

  <div id="loading"><?php
echo $TXT_OEREB_LOADING[$lang]; ?>
<div id="loading-spinner"></div>
</div>

  </div>

  <?php
  else: // $webserice läuft nicht

  $output=$TXT_OEREB_SERVICE_DOWN[$lang];


  echo $output;


  endif; // Ende $webservice==true
  ?>

  <?php

  /*****************************************************************
               Functions
  *****************************************************************/



  // Get remote file contents, preferring faster cURL if available
  function remote_get_contents($url)
  {
          if (function_exists('curl_get_contents') AND function_exists('curl_init'))
          {
                  return curl_get_contents($url);
          }
          else
          {
                  // A litte slower, but (usually) gets the job done
                  return file_get_contents($url);
          }
  }

  function curl_get_contents($url)
  {
          // Initiate the curl session
          $ch = curl_init();

          // Set the URL
          curl_setopt($ch, CURLOPT_URL, $url);

          // Without certificates
          // REMOVE BEFORE FLIGHT
          curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

          // Removes the headers from the output
          curl_setopt($ch, CURLOPT_HEADER, 0);

          // Return the output instead of displaying it directly
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

          // Execute the curl session
          $output = curl_exec($ch);

          // Close the curl session
          curl_close($ch);

          // Return the output as a variable
          return $output;
  }

  ?>
