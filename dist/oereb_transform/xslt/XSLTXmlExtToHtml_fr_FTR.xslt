<?xml version="1.0" encoding="utf-8"?>
<!--

 ***************************************************************
 	XSLT Stylesheeet fuer Footer ÖREB

 	Version 0.1 (18.09.2013)
 	das.zeichen (Bernhard Sturm)

  ***************************************************************

 HISTORY
 *******
 v0.1 18.09.2013	Erste Version

-->



<xsl:stylesheet version="1.0" exclude-result-prefixes="msxsl"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxsl="urn:schemas-microsoft-com:xslt"

>
				<xsl:output method="text" indent="no"/>
				<xsl:strip-space elements="*"/><!-- ROOT NODE -->



				<xsl:template match="Extraction"><!-- Header und Body -->

				<!-- Wichtige Variabeln -->
				<xsl:variable name="Kanton" select="//Kanton" />
				<xsl:variable name="Gemeinde" select="//Gemeinde" />
				<xsl:variable name="GemeindeNr" select="//Bfs" />
				<xsl:variable name="DateTimeCreated" select="//Erstellungsdatum" />
				<xsl:variable name="DateTimeCreatedFormat">
					<xsl:choose>
						<xsl:when test="$DateTimeCreated !='0000-00-00T00:00:00'">
							                        <xsl:call-template name="FormatDate">
								                                <xsl:with-param name="DateTime" select="$DateTimeCreated"/>
								           </xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text></xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>




<!--

************************************************

Inline Styles für den Footer, immer die aktuellste Version aus footer.css kopieren!

**************************************************
-->
					<![CDATA[

				<style type="text/css">
@font-face{font-family:'Cadastra';src:url('cadastra-webfont.woff2') format('woff2'),url('cadastra-webfont.woff') format('woff');font-weight:normal;font-style:normal}@font-face{font-family:'CadastraBd';src:url('cadastrabd-webfont.woff2') format('woff2'),url('cadastrabd-webfont.woff') format('woff');font-weight:normal;font-style:normal}@font-face{font-family:'CadastraCond';src:url('cadastracond-webfont.woff2') format('woff2'),url('cadastracond-webfont.woff') format('woff');font-weight:normal;font-style:normal}.pagebreak{page-break-after:always;display:block}.transparent{zoom:1;filter:alpha(opacity=70);opacity:.7}.footer{width:174mm;margin-left:18mm;margin-right:18mm;position:relative;font-family:'Cadastra',"Cadastra",Arial,Helvetica,sans-serif;font-weight:normal;font-size:8.66666667px;border-top:.282222mm solid #000;padding-top:.3mm}.footer span{display:inline-block;position:relative;top:0}.footer span.footer-date-value{left:0}.footer span.footer-id-value{margin-left:2.4mm}.footer span.footer-page-value{right:0;position:absolute;top:.3mm;display:block}

</style>


]]>




				<!-- START FOOTER-->
				<xsl:call-template name="template_footer" />
				<!-- ENDE FOOTER -->


			</xsl:template><!-- ENDE ROOT NODE -->

				<!-- DEFINTION DER TEMPLATES -->
				<!-- START FUSSZEILE -->
				<xsl:variable name="DateTimeCreated" select="//Erstellungsdatum" />
				<xsl:variable name="DateTimeCreatedFormat">
					<xsl:choose>
						<xsl:when test="$DateTimeCreated !='0000-00-00T00:00:00'">

							                        <xsl:call-template name="FormatDate">
								                                <xsl:with-param name="DateTime" select="$DateTimeCreated"/>
								           </xsl:call-template>

						</xsl:when>
						<xsl:otherwise>
							<xsl:text></xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>




				<xsl:template name="template_footer">

<![CDATA[
				<div class="footer" >
					<span class="footer-date-value">]]><xsl:value-of select="$DateTimeCreatedFormat" /><![CDATA[</span>
					<span class="footer-id-value">]]><xsl:value-of select="//ExtHeader/Info/Id" /><![CDATA[</span>

					<span class="footer-page-value">Page {page_number}/{total_pages}</span>
				</div>
				]]>

				</xsl:template>
				<!-- ENDE FUSSZEILE -->






				<!-- Template für Lower / Uppercase -->
				 <xsl:template name="ToLower">
				                <xsl:param name="inputString"/>
				                <xsl:variable name="smallCase" select="'abcdefghijklmnopqrstuvwxyzäöü'"/>
				                <xsl:variable name="upperCase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ'"/>
				                <xsl:value-of select="translate($inputString,$upperCase,$smallCase)"/>
				 </xsl:template>

				 <xsl:template name="ToUpper">
				                <xsl:param name="inputString"/>
				                <xsl:variable name="smallCase" select="'abcdefghijklmnopqrstuvwxyzäöü'"/>
				                <xsl:variable name="upperCase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ'"/>

				                <xsl:value-of select="translate($inputString,$smallCase,$upperCase)"/>

				   </xsl:template>


<!-- DATUMSFORMATE -->

        <xsl:template name="FormatDate">
				        <xsl:param name="DateTime" />
				        <xsl:variable name="mo">
				                        <xsl:value-of select="substring($DateTime,6,2)" />
				        </xsl:variable>
				        <xsl:variable name="day">
				                        <xsl:value-of select="substring($DateTime,9,2)" />
				          </xsl:variable>
				        <xsl:variable name="year">
				                        <xsl:value-of select="substring($DateTime,1,4)" />
				        </xsl:variable>
				        <xsl:variable name="time">
				                        <xsl:value-of select="substring($DateTime,12,8)" />
				        </xsl:variable>
				        <xsl:variable name="hh">
				                        <xsl:value-of select="substring($time,1,2)" />
				        </xsl:variable>
				        <xsl:variable name="mm">
				                        <xsl:value-of select="substring($time,4,2)" />
				        </xsl:variable>
				        <xsl:variable name="ss">
				                        <xsl:value-of select="substring($time,7,2)" />
				        </xsl:variable>
				                <xsl:value-of select="$day"/>
				              	 <xsl:value-of select="'.'"/>
				                <xsl:value-of select="$mo"/>
				               <xsl:value-of select="'.'"/>
				                <xsl:value-of select="$year"/>
				                <xsl:value-of select="', '"/>
				                <xsl:value-of select="$hh"/>
				               <xsl:value-of select="':'"/>
				                <xsl:value-of select="$mm"/>
				                <xsl:value-of select="':'"/>
				                <xsl:value-of select="$ss"/>
				        </xsl:template>


</xsl:stylesheet>
