<?xml version="1.0" encoding="utf-8"?>
<!--

 ***************************************************************
 	XSLT Stylesheeet fuer ÖREB-Kataster Auszüge
  Sprachversion FRANZÖSISCH

  Version siehe Bitbucket (https://bitbucket.org/stubr/oerebk_be16)
 	STURM UND BRÄM 2016

  ***************************************************************

-->


<xsl:stylesheet version="1.0" exclude-result-prefixes="msxsl" extension-element-prefixes="date" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:date="http://exslt.org/dates-and-times">

<!-- Hier folgen alle Sprachvariabeln auf FRANZÖSISCH -->
<xsl:variable name="DokumentAutor">Canton de Berne, Office de l'information géographique</xsl:variable>
<xsl:variable name="TextTitel">Extrait du cadastre des restrictions de droit public à la propriété foncière (cadastre RDPPF)</xsl:variable>
<xsl:variable name="TextLiegenschaftNr">No de l'immeuble</xsl:variable>
<xsl:variable name="TextGemeinde">Commune</xsl:variable>
<xsl:variable name="TextEGRIDNr">E-GRID</xsl:variable>
<xsl:variable name="TextLiegenschaftsflaeche">Surface</xsl:variable>
<xsl:variable name="TextBFS">N° OFS</xsl:variable>
<xsl:variable name="TextDatumAuszug">Extrait établi le</xsl:variable>
<xsl:variable name="TextErstellerAuszug">Organisme responsable du cadastre</xsl:variable>
<xsl:variable name="TextUnterschrift">Signature</xsl:variable>
<xsl:variable name="TextInhaltsverzeichnis">Sommaire des thèmes RDPPF</xsl:variable>
<xsl:variable name="TextSeite">Page</xsl:variable>
<xsl:variable name="TextAnhaenge">Liste des annexes</xsl:variable>
<xsl:variable name="TextZustaendigeStelle">Service compétent</xsl:variable>
<xsl:variable name="TextGlossar">Glossaire/abréviations</xsl:variable>
<xsl:variable name="TextSignatur">Signature</xsl:variable>
<xsl:variable name="TextIDNr">Identifiant de l'extrait</xsl:variable>
<xsl:variable name="TextTocKeineBeschraenkung">Restrictions de droit public à la propriété foncière qui ne touchent pas l'immeuble</xsl:variable>
<xsl:variable name="TextTocBeschraenkung">Restrictions de droit public à la propriété foncière qui touchent l'immeuble</xsl:variable>
<xsl:variable name="TextTocBeschraenkungNichtVerfuegbar">Restrictions de droit public à la propriété foncière pour lesquelles aucune donnée n’est disponible</xsl:variable>
<xsl:variable name="TextKreisBez">Arrondissement</xsl:variable>
<xsl:variable name="TextNorthArrow">Nord</xsl:variable>
<xsl:variable name="TextTitelBeglaubigung">Certification</xsl:variable>
<xsl:variable name="TextBeglaubigungArtikel">Selon OiOCRDP Art.3</xsl:variable>
<xsl:variable name="TextBeglaubigungURL">https://www.belex.sites.be.ch/frontend/texts_of_law/247?locale=fr</xsl:variable>
<xsl:variable name="TextStempel">Tampon</xsl:variable>
<xsl:variable name="TextDatum">Date</xsl:variable>
<xsl:variable name="TextTocIn"> de </xsl:variable>
<xsl:variable name="TextTocBetreffen"></xsl:variable>
<xsl:variable name="TextLegendeBeteiligtObjekt">Légende de la RDPPF concernée</xsl:variable>
<xsl:variable name="TextLegendeSichtbar">Autre légende (visible dans le cadre de plan)</xsl:variable>
<xsl:variable name="TextLegendeVollstaendig">Légende complète</xsl:variable>
<xsl:variable name="TextTitelAnteil">Part</xsl:variable>
<xsl:variable name="TextTitelFlaeche">Surface</xsl:variable>
<xsl:variable name="TextTitelAnzahl">Nombre</xsl:variable>
<xsl:variable name="TextTitelLaenge">Longueur</xsl:variable>
<xsl:variable name="TextTitelTyp">Type</xsl:variable>
<xsl:variable name="TextTitelGeneralInformation">Informations générales</xsl:variable>
<xsl:variable name="TextTitelBaseData">Données de base</xsl:variable>

</xsl:stylesheet>
