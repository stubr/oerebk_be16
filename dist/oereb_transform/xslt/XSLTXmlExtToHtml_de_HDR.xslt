<?xml version="1.0" encoding="utf-8"?>
<!--

 ***************************************************************
 	XSLT Stylesheeet fuer Header ÖREB

 	Version 1.0 (25.05.2016)
 	das.zeichen (Bernhard Sturm)
 	Sturm und Bräm GmbH (Bernhard Sturm)

  ***************************************************************

 HISTORY
 *******
 v0.1 18.09.2013	Erste Version
 v0.2 06.12.2013	Anpassungen Kantonswappen-Bern
 v0.3 12.12.2013	Anpassungen Text Kantonswappen-Bern
 v1.0 25.05.2016	Neue Version ab jetzt in bitbucket-Repo
-->



<xsl:stylesheet version="1.0" exclude-result-prefixes="msxsl"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxsl="urn:schemas-microsoft-com:xslt"

>
				<xsl:output method="text" indent="no"/>
				<xsl:strip-space elements="*"/><!-- ROOT NODE -->



				<xsl:template match="Extraction"><!-- Header und Body -->

				<!-- Wichtige Variabeln -->
				
				<!-- Alte Variabeln -->
				<xsl:variable name="Kanton" select="//Kanton" />
				<xsl:variable name="Gemeinde" select="//Gemeinde" />
				<xsl:variable name="GemeindeNr" select="//Bfs" />
				<xsl:variable name="DateTimeCreated" select="//Erstellungsdatum" />
				<xsl:variable name="DateTimeCreatedFormat">
					<xsl:choose>
						<xsl:when test="$DateTimeCreated !='0000-00-00T00:00:00'">

							<xsl:call-template name="FormatDate">
								<xsl:with-param name="DateTime" select="$DateTimeCreated"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text></xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>




<!-- 

************************************************			

Inline Styles für den Header, immer die aktuellste Version aus header.css kopieren!

**************************************************
-->
					<![CDATA[

				<style type="text/css">
@font-face{font-family:'Cadastra';src:url('cadastra-webfont.woff2') format('woff2'),url('cadastra-webfont.woff') format('woff');font-weight:normal;font-style:normal}@font-face{font-family:'CadastraBd';src:url('cadastrabd-webfont.woff2') format('woff2'),url('cadastrabd-webfont.woff') format('woff');font-weight:normal;font-style:normal}@font-face{font-family:'CadastraCond';src:url('cadastracond-webfont.woff2') format('woff2'),url('cadastracond-webfont.woff') format('woff');font-weight:normal;font-style:normal}.pagebreak{page-break-after:always;display:block}.transparent{zoom:1;filter:alpha(opacity=70);opacity:.7}.header{width:174mm;height:19mm;margin-top:10mm;margin-left:18mm;margin-right:18mm;position:relative;font-family:'Cadastra',"Cadastra",Arial,Helvetica,sans-serif;font-weight:normal;font-size:11.333333px;border-bottom:.266667px solid #000}.header .logo{position:absolute;top:0;left:0}.header .logo img{max-width:100%}.header .logo.eidgenossenschaft{left:0;width:44mm}.header .logo.kanton,.header .logo.gemeinde{height:13mm;left:60mm;width:30mm}.header .logo.kanton img,.header .logo.gemeinde img{text-align:center;height:13mm;width:30mm;object-fit:contain;object-position:center center}.header .logo.gemeinde{left:95mm}.header .logo.gemeinde img{height:9mm}.header .logo.gemeinde p{font-size:11.333333px;margin-top:1mm;line-height:100%;text-align:center}.header .logo.oereb{height:10mm;width:35mm;left:139mm}

</style>
			

]]>



				<!-- START HEADER -->
				<xsl:call-template name="template_header" />
				<!-- ENDE HEADER -->


			</xsl:template><!-- ENDE ROOT NODE -->

				<!-- DEFINTION DER TEMPLATES -->



				<!-- START HEADER -->
				<xsl:template name="template_header">
				
				<!-- XML.CH Variabeln -->
				<xsl:variable name="ImageCH" select="//ImageCH" />
				<xsl:variable name="ImageKanton" select="//ImageKanton" />
				<xsl:variable name="ImageGemeinde" select="//ImageGemeinde" />
				<xsl:variable name="ImageOereb" select="//ImageOereb" />
				<xsl:variable name="Gemeinde" select="//Gemeinde" />

				<![CDATA[

					<div class="header">
						<div class="logo eidgenossenschaft debug-this">
							<img src="]]><xsl:value-of select="$ImageCH" /><![CDATA[" />


						</div>
						<div class="logo kanton debug-this">
							<img src="]]><xsl:value-of select="$ImageKanton" /><![CDATA[" />

						</div>
						<div class="logo gemeinde debug-this">
							<img src="]]><xsl:value-of select="$ImageGemeinde" /><![CDATA[" />
							<p>]]><xsl:value-of select="$Gemeinde"/><![CDATA[</p>
						</div>
						<div class="logo oereb debug-this">
							<img src="]]><xsl:value-of select="$ImageOereb" /><![CDATA[" />

						</div>
					</div>
					]]>

				</xsl:template>
				<!-- ENDE HEADER -->




				<!-- Template für Lower / Uppercase -->
				 <xsl:template name="ToLower">
				                <xsl:param name="inputString"/>
				                <xsl:variable name="smallCase" select="'abcdefghijklmnopqrstuvwxyzäöü'"/>
				                <xsl:variable name="upperCase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ'"/>
				                <xsl:value-of select="translate($inputString,$upperCase,$smallCase)"/>
				 </xsl:template>

				 <xsl:template name="ToUpper">
				                <xsl:param name="inputString"/>
				                <xsl:variable name="smallCase" select="'abcdefghijklmnopqrstuvwxyzäöü'"/>
				                <xsl:variable name="upperCase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ'"/>

				                <xsl:value-of select="translate($inputString,$smallCase,$upperCase)"/>

				   </xsl:template>


<!-- DATUMSFORMATE -->

        <xsl:template name="FormatDate">
				        <xsl:param name="DateTime" />
				        <xsl:variable name="mo">
				                        <xsl:value-of select="substring($DateTime,6,2)" />
				        </xsl:variable>
				        <xsl:variable name="day">
				                        <xsl:value-of select="substring($DateTime,9,2)" />
				          </xsl:variable>
				        <xsl:variable name="year">
				                        <xsl:value-of select="substring($DateTime,1,4)" />
				        </xsl:variable>
				        <xsl:variable name="time">
				                        <xsl:value-of select="substring($DateTime,12,8)" />
				        </xsl:variable>
				        <xsl:variable name="hh">
				                        <xsl:value-of select="substring($time,1,2)" />
				        </xsl:variable>
				        <xsl:variable name="mm">
				                        <xsl:value-of select="substring($time,4,2)" />
				        </xsl:variable>
				        <xsl:variable name="ss">
				                        <xsl:value-of select="substring($time,7,2)" />
				        </xsl:variable>
				                <xsl:value-of select="$day"/>
				              	 <xsl:value-of select="'.'"/>
				                <xsl:value-of select="$mo"/>
				               <xsl:value-of select="'.'"/>
				                <xsl:value-of select="$year"/>
				                <xsl:value-of select="' '"/>
				                <xsl:value-of select="$hh"/>
				               <xsl:value-of select="':'"/>
				                <xsl:value-of select="$mm"/>
				                <xsl:value-of select="':'"/>
				                <xsl:value-of select="$ss"/>
				        </xsl:template>


</xsl:stylesheet>
