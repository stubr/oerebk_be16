<?xml version="1.0" encoding="utf-8"?>
<!--

 ***************************************************************
 	XSLT Stylesheeet fuer ÖREB-Kataster Auszüge
  Sprachversion DEUTSCH

 	Version 0.2 (19.08.2013)
 	das.zeichen (Bernhard Sturm)

  ***************************************************************

 HISTORY
 *******
 v0.1	08.08.2013  Start Implementierung PDF-Vorlage		
 v0.2 19.08.2013  Einfügen der Text	
 
-->



<xsl:stylesheet version="1.0" exclude-result-prefixes="msxsl" extension-element-prefixes="date" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:date="http://exslt.org/dates-and-times">

<!-- Hier folgen alle Sprachvariabeln auf DEUTSCH -->
<xsl:variable name="Sprache">fr</xsl:variable>
<xsl:variable name="DokumentAutor">Kanton Bern, Amt für Geoinformation</xsl:variable>
<xsl:variable name="DokumentTitelPrefix">ÖREB - Kataster, Kanton Bern</xsl:variable>

<!-- LOGOS -->
<xsl:variable name="LogoNameEidgenossenschaft">Schweizerische Eidgenossenschaft</xsl:variable>

<!-- ALLGMEINE TEXTE UND LABELS -->
<xsl:variable name="TextTitel">Extrait du cadastre des restrictions de droit public à la propriété foncière</xsl:variable>
<xsl:variable name="TextTitelBeglaubigt">Extrait officiel du cadastre des restrictions de droit public à la propriété foncière</xsl:variable>
<xsl:variable name="TextLiegenschaftNr">Bien-fonds n°</xsl:variable>
<xsl:variable name="TextFlurname">Nom local</xsl:variable>
<xsl:variable name="TextGemeinde">Commune</xsl:variable>
<xsl:variable name="TextEGRIDNr">N° EGRID</xsl:variable>
<xsl:variable name="TextLiegenschaftsflaeche">Surface du bien-fonds</xsl:variable>
<xsl:variable name="TextBFS">N° OFS</xsl:variable>
<xsl:variable name="TextDatumAuszug">Extrait établi le:</xsl:variable>
<xsl:variable name="TextErstellerAuszug">Editeur de l'extrait:</xsl:variable>
<xsl:variable name="TextUnterschrift">Signature</xsl:variable>
<xsl:variable name="TextHinweise">Indications générales</xsl:variable>
<xsl:variable name="TextInhaltsverzeichnis">Table des matières</xsl:variable>
<xsl:variable name="TextSeite">Page</xsl:variable>
<xsl:variable name="TextEigentumsbeschraenkung">Restriction</xsl:variable>
<xsl:variable name="TextIdentifikationsnr">Identificateur</xsl:variable>
<xsl:variable name="TextRechtsvorschriften">Dispositions juridiques</xsl:variable>
<xsl:variable name="TextWeitereHinweise">Infos et renvois supplémentaires</xsl:variable>
<xsl:variable name="TextMetadaten">Métadonnées</xsl:variable>
<xsl:variable name="TextNutzungsplanung">Plans d’affectation (cantonaux / communaux)</xsl:variable>
<xsl:variable name="TextLaermempfindlichkeit">Degré de sensibilité au bruit (dans les zones d’affectation)</xsl:variable>
<xsl:variable name="TextWaldgrenzen">Limites de la forêt (dans les zones à bâtir)</xsl:variable>
<xsl:variable name="TextWaldabstand">Distances par rapport à la forêt</xsl:variable>
<xsl:variable name="TextBelasteteStandorte">Cadastre des sites pollués</xsl:variable>
<xsl:variable name="TextGrundwasserschutzZonnen">Zones de protection des eaux souterraines</xsl:variable>
<xsl:variable name="TextGrundwasserschutzAreale">Périmètres de protection des eaux souterraines</xsl:variable>
<xsl:variable name="TextNationalstrassen">Alignements des routes nationales</xsl:variable>
<xsl:variable name="TextKatasterMilitaer">Cadastre des sites pollués – domaine militaire</xsl:variable>
<xsl:variable name="TextKatasterZivilFlgplz">Cadastre des sites pollués – domaine des aérodromes civils</xsl:variable>
<xsl:variable name="TextKatasterOeV">Cadastre des sites pollués – domaine des transports publics</xsl:variable>
<xsl:variable name="TextProjektNationalstrassen">Zones réservées des routes nationales</xsl:variable>
<xsl:variable name="TextProjektFlgplz">Zones réservées des installations aéroportuaires</xsl:variable>
<xsl:variable name="TextBaulinieFlgplz">Alignements des installations aéroportuaires</xsl:variable>
<xsl:variable name="TextSicherungFlgplz">Plan de la zone de sécurité des aéroports</xsl:variable>
<xsl:variable name="TextProjektEisenbahn">Zones réservées des installations ferroviaires</xsl:variable>
<xsl:variable name="TextBaulinieEisenbahn">Alignements des installations ferroviaires</xsl:variable>
<xsl:variable name="TextUnverbindlicheInformation">L'information n'est pas contraignante.</xsl:variable>
<xsl:variable name="TextKeineBeschraenkung">Le bien-fonds n'est pas touché par cette restriction.</xsl:variable>
<xsl:variable name="TextAnhang1">Conformément à l'annexe 1 de l'OCRDP</xsl:variable>
<xsl:variable name="TextReihenfolgeAnhang">Les indications font référence à la liste des annexes</xsl:variable>
<xsl:variable name="TextAnhaenge">Liste des annexes</xsl:variable>
<xsl:variable name="TextGrundbuchplan">Plan du registre foncier</xsl:variable>
<xsl:variable name="TextLegende">Légende</xsl:variable>
<xsl:variable name="TextCharakteristik">Caractéristique</xsl:variable>
<xsl:variable name="TextZustaendigeStelle">Service compétent</xsl:variable>
<xsl:variable name="TextGrundlagen">Base(s) légale(s)</xsl:variable>
<xsl:variable name="TextAenderungen">Modifications en cours</xsl:variable>
<xsl:variable name="TextNichtVerfuegbar">Cette restriction n'est pas disponible. Veuillez vous adresser au service compétent pour obtenir plus d'information.</xsl:variable>
<xsl:variable name="TextNichtVerbindlich">L'information concernant cettre restriction n'est pas contraignante.</xsl:variable>
<xsl:variable name="TextGlossar">Glossaire</xsl:variable>
<xsl:variable name="TextAnhang">Annexe</xsl:variable>
<xsl:variable name="TextAdresse">Adresse/Nom local</xsl:variable>
<xsl:variable name="TextSignatur">Signature</xsl:variable>
<xsl:variable name="TextIDNr">ID Nr</xsl:variable>
<xsl:variable name="TextTocKeineBeschraenkung">Les restrictions suivantes ne touchent pas l'immeuble</xsl:variable>
<xsl:variable name="TextTocBeschraenkung">FR Beschränkung</xsl:variable>
<xsl:variable name="TextTocBeschraenkungNichtVerfuegbar">Les restrictions suivantes ne sont pas disponibles</xsl:variable>
<xsl:variable name="TextRechtsstatus">FR Rechtsstatus</xsl:variable>
<xsl:variable name="TextTypedRegulationsREVO">Dispositions légales</xsl:variable>
<xsl:variable name="TextTypedRegulationsHINW">Informations et renvois supp.</xsl:variable>

</xsl:stylesheet>
