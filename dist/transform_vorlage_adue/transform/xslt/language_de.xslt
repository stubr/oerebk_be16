<?xml version="1.0" encoding="utf-8"?>
<!--

 ***************************************************************
 	XSLT Stylesheeet fuer ÖREB-Kataster Auszüge
  Sprachversion DEUTSCH

 	Version 0.2 (19.08.2013)
 	das.zeichen (Bernhard Sturm)

  ***************************************************************

 HISTORY
 *******
 v0.1	08.08.2013  Start Implementierung PDF-Vorlage		
 v0.2 19.08.2013  Einfügen der Text	
 
-->



<xsl:stylesheet version="1.0" exclude-result-prefixes="msxsl" extension-element-prefixes="date" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:date="http://exslt.org/dates-and-times">

<!-- Hier folgen alle Sprachvariabeln auf DEUTSCH -->
<xsl:variable name="Sprache">de</xsl:variable>
<xsl:variable name="DokumentAutor">Kanton Bern, Amt für Geoinformation</xsl:variable>
<xsl:variable name="DokumentTitelPrefix">ÖREB - Kataster, Kanton Bern</xsl:variable>

<!-- LOGOS -->
<xsl:variable name="LogoNameEidgenossenschaft">Schweizerische Eidgenossenschaft</xsl:variable>

<!-- ALLGMEINE TEXTE UND LABELS -->
<xsl:variable name="TextTitel">Auszug aus dem Kataster der öffentlich-rechtlichen Eigentumsbeschränkungen</xsl:variable>
<xsl:variable name="TextTitelBeglaubigt">Beglaubigter Auszug aus dem Kataster der öffentlich-rechtlichen Eigentumsbeschränkungen</xsl:variable>
<xsl:variable name="TextLiegenschaftNr">Liegenschaft Nr.</xsl:variable>
<xsl:variable name="TextFlurname">Flurname</xsl:variable>
<xsl:variable name="TextGemeinde">Gemeinde</xsl:variable>
<xsl:variable name="TextEGRIDNr">EGRID Nr.</xsl:variable>
<xsl:variable name="TextLiegenschaftsflaeche">Liegenschaftsfläche</xsl:variable>
<xsl:variable name="TextBFS">BFS Nr.</xsl:variable>
<xsl:variable name="TextDatumAuszug">Auszug erstellt am:</xsl:variable>
<xsl:variable name="TextErstellerAuszug">Ersteller des Auszuges:</xsl:variable>
<xsl:variable name="TextUnterschrift">Unterschrift</xsl:variable>
<xsl:variable name="TextHinweise">Allgemeine Hinweise</xsl:variable>
<xsl:variable name="TextInhaltsverzeichnis">Inhaltsverzeichnis</xsl:variable>
<xsl:variable name="TextSeite">Seite</xsl:variable>
<xsl:variable name="TextEigentumsbeschraenkung">Eigentumsbeschränkung</xsl:variable>
<xsl:variable name="TextIdentifikationsnr">Identifikationsnummer</xsl:variable>
<xsl:variable name="TextRechtsvorschriften">Rechtsvorschriften</xsl:variable>
<xsl:variable name="TextWeitereHinweise">Weitere Informationen und Hinweise</xsl:variable>
<xsl:variable name="TextMetadaten">Metadaten</xsl:variable>
<xsl:variable name="TextNutzungsplanung">Nutzungsplanung (kantonal / kommunal)</xsl:variable>
<xsl:variable name="TextLaermempfindlichkeit">Lärmempfindlichkeitsstufen (in Nutzungszonen)</xsl:variable>
<xsl:variable name="TextWaldgrenzen">Waldgrenzen (in Bauzonen)</xsl:variable>
<xsl:variable name="TextWaldabstand">Waldabstandslinien</xsl:variable>
<xsl:variable name="TextBelasteteStandorte">Kataster der belasteten Standorte</xsl:variable>
<xsl:variable name="TextGrundwasserschutzZonnen">Grundwasserschutzzonen</xsl:variable>
<xsl:variable name="TextGrundwasserschutzAreale">Grundwasserschutzareale</xsl:variable>
<xsl:variable name="TextNationalstrassen">Baulinien Nationalstrassen</xsl:variable>
<xsl:variable name="TextKatasterMilitaer">Kataster der belasteten Standorte im Bereich des Militärs</xsl:variable>
<xsl:variable name="TextKatasterZivilFlgplz">Kataster der belasteten Standorte im Bereich der zivilen Flugplätze</xsl:variable>
<xsl:variable name="TextKatasterOeV">Kataster der belasteten Standorte im Bereich des öffentlichen Verkehrs</xsl:variable>
<xsl:variable name="TextProjektNationalstrassen">Projektierungszonen Nationalstrassen</xsl:variable>
<xsl:variable name="TextProjektFlgplz">Projektierungszonen Flughafenanlagen</xsl:variable>
<xsl:variable name="TextBaulinieFlgplz">Baulinien Flughafenanlagen</xsl:variable>
<xsl:variable name="TextSicherungFlgplz">Sicherheitszonenplan bei Flughäfen</xsl:variable>
<xsl:variable name="TextProjektEisenbahn">Projektierungszonen Eisenbahnanlagen</xsl:variable>
<xsl:variable name="TextBaulinieEisenbahn">Baulinien Eisenbahnanlagen</xsl:variable>
<xsl:variable name="TextUnverbindlicheInformation">Unverbindliche Information</xsl:variable>
<xsl:variable name="TextKeineBeschraenkung">Die Liegenschaft ist von der Eigentumsbeschränkung nicht betroffen.</xsl:variable>
<xsl:variable name="TextAnhang1">Gemäss GeoIV Anhang 1</xsl:variable>
<xsl:variable name="TextReihenfolgeAnhang">Die Angaben verweisen auf die Reihenfolge der Anhänge</xsl:variable>
<xsl:variable name="TextAnhaenge">Anhänge</xsl:variable>
<xsl:variable name="TextGrundbuchplan">Grundbuchplan</xsl:variable>
<xsl:variable name="TextLegende">Legende</xsl:variable>
<xsl:variable name="TextCharakteristik">Charakteristik</xsl:variable>
<xsl:variable name="TextZustaendigeStelle">Zuständige Stelle</xsl:variable>
<xsl:variable name="TextGrundlagen">Gesetzliche Grundlagen</xsl:variable>
<xsl:variable name="TextAenderungen">Laufende Änderungen</xsl:variable>
<xsl:variable name="TextNichtVerfuegbar">Die Eigentumsbeschränkung ist momentan nicht verfügbar. Kontaktieren Sie die zuständige Stelle für weitere Informationen.</xsl:variable>
<xsl:variable name="TextNichtVerbindlich">Die Information bezüglich der Eigentumsbeschränkung ist nicht verbindlich.</xsl:variable>
<xsl:variable name="TextGlossar">Glossar</xsl:variable>
<xsl:variable name="TextAnhang">Anhang</xsl:variable>
<xsl:variable name="TextAdresse">Adresse</xsl:variable>
<xsl:variable name="TextSignatur">Unterschrift</xsl:variable>
<xsl:variable name="TextIDNr">ID-Nr.</xsl:variable>
<xsl:variable name="TextTocKeineBeschraenkung">Keine Beschränkung</xsl:variable>
<xsl:variable name="TextTocBeschraenkung">Beschränkung</xsl:variable>
<xsl:variable name="TextTocBeschraenkungNichtVerfuegbar">Beschränkung nicht verfügbar</xsl:variable>
<xsl:variable name="TextRechtsstatus">Rechtsstatus</xsl:variable>
<xsl:variable name="TextTypedRegulationsREVO">Gesetzliche Bestimmungen</xsl:variable>
<xsl:variable name="TextTypedRegulationsHINW">Weitere Informationen und Referenzen</xsl:variable>

</xsl:stylesheet>
