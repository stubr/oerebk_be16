<?xml version="1.0" encoding="utf-8"?>
<!--

 ***************************************************************
 	XSLT Stylesheeet fuer ÖREB-Kataster Auszüge

 	Version 1.0 (19.09.2013)
 	das.zeichen (Bernhard Sturm)

  ***************************************************************

 HISTORY
 *******
 v0.1 17.05.2013 Erstes Release, basierend auf Muster-XML a-due
 v0.2 08.08.2013  Start Implementierung PDF-Vorlage		
 v0.3 19.08.2013  Implementierung LOGOS, Header	
 v0.4 23.08.2013  Implementierung Deckblatt
 v0.5 03.09.2013  Implementierung TOC/Appendix
 v0.6 06.09.2013  Implementierung Auflagen
 v0.7 06.09.2013  Externe Ressourcen für LESS / JS / BILDER
 v0.8 10.09.2013  Dates and Times als Template für XSLT 1.0 realisiert / Anpassungen
 v0.9 17.09.2013 Header / Footer unterdrückt
 v1.0 19.09.2013 Seitennummerieung für Inhaltsverzeichnis eingefügt
 
-->



<xsl:stylesheet version="1.0" exclude-result-prefixes="msxsl"  
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxsl="urn:schemas-microsoft-com:xslt" 
	
>
				<xsl:output method="html" indent="yes"/>
				<xsl:strip-space elements="*"/><!-- ROOT NODE -->
				
				
				
				<!-- Include Sprachvariabeln (abhängig der Sprachversion 
				A C H T U N G ! ! Dieser Wert muss manuell gesetzt werden
				-->			
				<xsl:include href="language_fr.xslt" />

				
				<xsl:template match="Extraction"><!-- Header und Body -->
				
				<!-- Wichtige Variabeln -->
				<xsl:variable name="Kanton" select="//Kanton" />
				<xsl:variable name="Gemeinde" select="//Gemeinde" />
				<xsl:variable name="GemeindeNr" select="//Bfs" />
				<xsl:variable name="Reduziert" select="//IstReduziert" />
				<xsl:variable name="DateTimeCreated" select="//Erstellungsdatum" />
				<xsl:variable name="DateTimeCreatedFormat">
					<xsl:choose>
						<xsl:when test="$DateTimeCreated !='0000-00-00T00:00:00'">
							                        <xsl:call-template name="FormatDate">
								                                <xsl:with-param name="DateTime" select="$DateTimeCreated"/>
								           </xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text></xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				
				
				<html>
					<head>
						<meta name="author" content="{$DokumentAutor}"/>
						<meta http-equiv="Pragma" content="no-cache"/>
						<meta http-equiv="Expires" content="-1"/>
						
						<link rel="stylesheet" type="text/css" href="oereb.css"/>
					
					</head>
					<body>
						<!-- START HEADER -->
						<xsl:call-template name="template_header" />
						<!-- ENDE HEADER -->
						<!-- START DECKBLATT -->
						<div class="page deckblatt">
							<xsl:choose>
									<!-- Beglaubigt oder nicht beglaubigt -->		
									<xsl:when test="$Reduziert='true'">
							 			<h1><xsl:value-of select="$TextTitel"/></h1>
									</xsl:when>
									<xsl:otherwise>
										<h1><xsl:value-of select="$TextTitelBeglaubigt"/></h1>		
									</xsl:otherwise>
							</xsl:choose>
							<div class="map">
								<img>
									<xsl:attribute name="src"><xsl:value-of select="//Parcel/VerweisWMS" /></xsl:attribute>
									<xsl:attribute name="alt"></xsl:attribute>
								</img>
								<div class="mapkey">
									<img>
										<xsl:attribute name="src"><xsl:value-of select="//Parcel/LegendeImWeb" /></xsl:attribute>
										<xsl:attribute name="alt"><xsl:value-of select="$TextLegende" /></xsl:attribute>
									</img>
								</div>
							</div>
							<div class="info">
								<div class="row">
									<span class="label"><xsl:value-of select="$TextLiegenschaftNr" /></span>
									<span class="value"><xsl:value-of select="//Nummer" /></span>
								</div>		
								<div class="row cols clearfix">
									<div class="col">
										<span class="label"><xsl:value-of select="$TextLiegenschaftsflaeche" /></span>
										<span class="value"><xsl:value-of select="round(//Area*100) div 100" />m<span class="upper">2</span></span>
									</div>
									<div class="col">
										<span class="label"><xsl:value-of select="$TextEGRIDNr" /></span>
										<span class="value"><xsl:value-of select="//Egrid" /></span>
									</div>
								</div>		
								<div class="row">
									<span class="label"><xsl:value-of select="$TextGemeinde" /> (<xsl:value-of select="$TextBFS" />)</span>
									<span class="value"><xsl:value-of select="//Gemeinde"/> (<xsl:value-of select="//Bfs" />)</span>
								</div>	
								

								<div class="row" id="signature" >
									<span class="label"><xsl:value-of select="$TextSignatur" /></span>
									<div  class="box-signature"></div>
								</div>	
								<!-- TO DO: SEITENUMBRUCH!  -->
								<div class="row" id="hinweise" >
									<h3><xsl:value-of select="$TextHinweise" /></h3>
									
									<xsl:for-each select="//Reservation">
									<p><xsl:value-of select="Vorbehalt"/> </p>
									</xsl:for-each>									
								</div>	
							 	
							</div>
						</div>
						<!-- ENDE DECKBLAT -->
						<!-- START FUSSZEILE -->
						<xsl:call-template name="template_footer" />
						<!-- ENDE FUSSZEILE -->
						
						<!-- START KOPFZEILE -->
						<xsl:call-template name="template_header" />
						<!-- ENDE KOPFZEILE -->
						
						 <!-- START INHALTSVERZEICHNIS -->
						 <div class="page toc">
						 	<h2><xsl:value-of select="$TextInhaltsverzeichnis" /></h2> 
							<h3><xsl:value-of select="$TextTocBeschraenkung" /> </h3>
							<table class="toc-table">
							<xsl:for-each select="//TocThemesWithIntersect/TocTheme">
							<tr>
								<td class="value"><xsl:value-of select="Name"/></td><td class="definition"><xsl:value-of select="//ThemePageNumber" /></td>
							</tr>
							</xsl:for-each>
							</table>
						               <h3><xsl:value-of select="$TextTocKeineBeschraenkung" /> </h3>
						               <xsl:for-each select="//TocThemesWithoutIntersect/TocTheme">
							<p><xsl:value-of select="Name"/></p>
							</xsl:for-each>
						               <h3><xsl:value-of select="$TextTocBeschraenkungNichtVerfuegbar" /> </h3>
						               <xsl:for-each select="//TocThemesNotAvailable/TocTheme">
							<p><xsl:value-of select="Name"/></p>
							</xsl:for-each>
						 
						 </div>
						 
						<!-- START FUSSZEILE -->
						<xsl:call-template name="template_footer" />
						<!-- ENDE FUSSZEILE -->
						 
						 <!-- ENDE INHALTSVERZEICHNIS -->
						 
						 						

						
						<!-- START BESCHRÄNKUNGEN -->
						
						<xsl:for-each select="//PropertyRestrictionInfo">
						
						
						<!-- START KOPFZEILE -->
						<xsl:call-template name="template_header" />
						<!-- ENDE KOPFZEILE -->
						<!-- EINE RESTRICTION PRO SEITE -->
						<div class="page restrictions" >
								<h2><xsl:value-of select="PropertyRestriction/Thema"/></h2>
								<div class="map">
									<img>
										<xsl:attribute name="src"><xsl:value-of select="//Parcel/VerweisWMS" /></xsl:attribute>
										<xsl:attribute name="alt"></xsl:attribute>
									</img>
									<img>
										<xsl:attribute name="src"><xsl:value-of select="PropertyRestriction/VerweisWMS" /></xsl:attribute>
										<xsl:attribute name="alt"></xsl:attribute>
									</img>


									<div class="mapkey">
										<img>
											<xsl:attribute name="src"><xsl:value-of select="PropertyRestriction/LegendeImWeb" /></xsl:attribute>
											<xsl:attribute name="alt"><xsl:value-of select="$TextLegende" /></xsl:attribute>
										</img>
									</div>
								</div>
								<div class="info">
									<div class="row">
										<span class="label"><xsl:value-of select="$TextCharakteristik" /></span>
										<span class="value"><xsl:value-of select="PropertyRestriction/Aussage" /> (<xsl:value-of select="round(IntersectArea*100) div 100" />m<span class="upper">2</span>)</span>
									</div>		
									<div class="row">
										<span class="label"><xsl:value-of select="$TextRechtsstatus" /></span>
										<span class="value"><xsl:value-of select="PropertyRestriction/Rechtsstatus" /></span>
									</div>	
									<xsl:if test="HasRegulations='true'">
										<xsl:for-each select="Regulations/TypedRegulationList/TypedRegulations">
											<xsl:if test="Type='REVO'">
												<div class="row">
													<span class="label"><xsl:value-of select="$TextTypedRegulationsREVO" /></span>
													<span class="value">
														<xsl:for-each select="RegulationList/Regulation">
															<xsl:value-of select="OffiziellerTitel"/><br/>
															<a>
																<xsl:attribute name="href"><xsl:value-of select="TextImWeb"/></xsl:attribute>
																<xsl:value-of select="TextImWeb"/>
															</a><p></p>	
																		
																
														</xsl:for-each>
													</span>
												</div>	
											</xsl:if>
										</xsl:for-each>	
										<xsl:for-each select="Regulations/TypedRegulationList/TypedRegulations">
											<xsl:if test="Type='HINW'">
												<div class="row">
													<span class="label"><xsl:value-of select="$TextTypedRegulationsHINW" /></span>
													<span class="value">
														<xsl:for-each select="RegulationList/Regulation">
															<xsl:value-of select="OffiziellerTitel"/><br/>
															<a>
																<xsl:attribute name="href"><xsl:value-of select="TextImWeb"/></xsl:attribute>
																<xsl:value-of select="TextImWeb"/>
															</a><p></p>	
																		
																
														</xsl:for-each>
													</span>
												</div>	
											</xsl:if>
										</xsl:for-each>											
									</xsl:if>
									<div class="row">
										<span class="label"><xsl:value-of select="$TextZustaendigeStelle" /></span>
										<span class="value"><xsl:value-of select="PropertyRestriction/Amt" />
										<br/>
										<a>
											<xsl:attribute name="href"><xsl:value-of select="PropertyRestriction/AmtImWeb"/></xsl:attribute>
											<xsl:value-of select="PropertyRestriction/AmtImWeb"/>
										</a>
										
										</span>
									</div>	
									
									<xsl:if test="HasRegulations='true'">
										<xsl:for-each select="Regulations/TypedRegulationList/TypedRegulations">
											<xsl:if test="Type='GEGR'">
												<div class="row">
													<span class="label"><xsl:value-of select="$TextGrundlagen" /></span>
													<span class="value">
														<xsl:for-each select="RegulationList/Regulation">
															<xsl:value-of select="OffiziellerTitel"/><br/>
															<a>
																<xsl:attribute name="href"><xsl:value-of select="TextImWeb"/></xsl:attribute>
																<xsl:value-of select="TextImWeb"/>
															</a><p></p>	
																		
																
														</xsl:for-each>
													</span>
												</div>	
											</xsl:if>
										</xsl:for-each>								
									</xsl:if>
										
												
								</div>
								
						
						</div>						
						<!-- START FUSSZEILE -->
						<xsl:call-template name="template_footer" />
						<!-- ENDE FUSSZEILE -->
						</xsl:for-each>
						<!-- ENDE BESCHRÄNKUNGEN -->
						
						
						<!-- START ANNEXE -->
						
						<!-- START KOPFZEILE -->
						<xsl:call-template name="template_header" />
						<!-- ENDE KOPFZEILE -->
						<div class="page annexe" >
						
						<h2><xsl:value-of select="$TextAnhaenge" /></h2>
						<xsl:for-each select="//Annex">
							<p><strong><xsl:value-of select="Title"/></strong></p>	
							<a>
								<xsl:attribute name="href"><xsl:value-of select="Uri" /></xsl:attribute>
								<xsl:value-of select="Uri" />
				
							</a>				
						</xsl:for-each>
						</div>						

						
						<!-- ENDE ANNEXE -->
						
						
<!-- AUSKOMMENTIERTE TEMPLATES
						<xsl:apply-templates/>
						AUSKOMMENTIERTE TEMPLATES -->
					</body>
				</html>
				</xsl:template><!-- ENDE ROOT NODE -->
				
				<!-- DEFINTION DER TEMPLATES -->
				<!-- START FUSSZEILE -->
				<xsl:variable name="DateTimeCreated" select="//Erstellungsdatum" />
				<xsl:variable name="DateTimeCreatedFormat">
					<xsl:choose>
						<xsl:when test="$DateTimeCreated !='0000-00-00T00:00:00'">
						
							                        <xsl:call-template name="FormatDate">
								                                <xsl:with-param name="DateTime" select="$DateTimeCreated"/>
								           </xsl:call-template>

						</xsl:when>
						<xsl:otherwise>
							<xsl:text></xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>

				
				
				
				<xsl:template name="template_footer">
				<!--
					<div class="footer" >
						<span class="footer-date-label"><xsl:value-of select="$TextDatumAuszug" /></span>
						<span class="footer-date-value"><xsl:value-of select="$DateTimeCreatedFormat" /></span>
						<span class="footer-id-label"><xsl:value-of select="$TextIDNr" /></span>
						<span class="footer-id-value"><xsl:value-of select="//Id" /></span>
						<span class="footer-page-label"><xsl:value-of select="$TextSeite" /></span>
						
						<span class="footer-page-value">5 von 7</span>
					</div>
					 SEITENUMBRUCH 
					 -->

					
					<div class="footer-break page-break"></div>
					
				
				</xsl:template>
				<!-- ENDE FUSSZEILE -->
				
				
				<!-- START HEADER -->
				<xsl:template name="template_header">
					<!-- Wichtige Variabeln -->
					<!--
					<xsl:variable name="Kanton" select="//Kanton" />
					<xsl:variable name="Gemeinde" select="//Gemeinde" />
					<xsl:variable name="GemeindeNr" select="//Bfs" />

				
					<div class="header">
						<div class="logo eidgenossenschaft">
							<img src="http://files.be.ch/bve/agi/oereb/logos/eidgenossenschaft.png" alt="{$LogoNameEidgenossenschaft}" />	
						</div>		
					<div class="logo kanton">
						<img>
							<xsl:attribute name="src"><xsl:value-of select="concat('http://files.be.ch/bve/agi/oereb/logos/kanton_',$Kanton,'.gif')" /></xsl:attribute>
							<xsl:attribute name="alt"><xsl:value-of select="$Kanton" /></xsl:attribute>
						</img> 
						</div>
						<div class="logo gemeinde">
							<img>
								<xsl:attribute name="src"><xsl:value-of select="concat('http://files.be.ch/bve/agi/oereb/logos/',$GemeindeNr,'.png')" /></xsl:attribute>
								<xsl:attribute name="alt"><xsl:value-of select="$Gemeinde" /></xsl:attribute>
							</img> 
							<p><xsl:value-of select="$Gemeinde"/></p>
						</div>		
					</div>
					-->
				</xsl:template>
				<!-- ENDE HEADER -->

				
				
				
<!-- AUSKOMMENTIERTER TEST 
				<xsl:template match="ExtHeader">
					<h2>Header</h2>
					<table>
						<xsl:apply-templates select="node()|@*"/>
					</table>
				</xsl:template>
				
				<xsl:template match="ExtBody">
					<h2>Body</h2>
					<table>
						<xsl:apply-templates select="node()|@*"/>
					</table>
				</xsl:template>
				
				<xsl:template match="ExtFooter">
					<h2>Footer</h2>
					<table>
						<xsl:apply-templates select="node()|@*"/>
					</table>
				</xsl:template>
				<xsl:template match="text()">
					<tr>
						<td class="definition">
							<xsl:value-of select="concat(name(parent::*),': ')"/>
						</td>
						<td class="value">
							<xsl:value-of select="."/>
						</td>
					</tr>
				</xsl:template>
				<xsl:template match="@*">
					<xsl:value-of select="concat(name(),'=&quot;',.,'&quot;')"/>
				</xsl:template>
				

				ENDE AUSKOMMENTIERTER TEST -->

				<!-- Template für Lower / Uppercase -->
				 <xsl:template name="ToLower">
				                <xsl:param name="inputString"/>
				                <xsl:variable name="smallCase" select="'abcdefghijklmnopqrstuvwxyzäöü'"/>
				                <xsl:variable name="upperCase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ'"/>
				                <xsl:value-of select="translate($inputString,$upperCase,$smallCase)"/>
				 </xsl:template>
				 
				 <xsl:template name="ToUpper">
				                <xsl:param name="inputString"/>
				                <xsl:variable name="smallCase" select="'abcdefghijklmnopqrstuvwxyzäöü'"/>
				                <xsl:variable name="upperCase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ'"/>
				            
				                <xsl:value-of select="translate($inputString,$smallCase,$upperCase)"/>
				 
				   </xsl:template>
				
				
<!-- DATUMSFORMATE -->
				
        <xsl:template name="FormatDate">
				        <xsl:param name="DateTime" />
				        <xsl:variable name="mo">
				                        <xsl:value-of select="substring($DateTime,6,2)" />
				        </xsl:variable>
				        <xsl:variable name="day">
				                        <xsl:value-of select="substring($DateTime,9,2)" />
				          </xsl:variable>
				        <xsl:variable name="year">
				                        <xsl:value-of select="substring($DateTime,1,4)" />
				        </xsl:variable>
				        <xsl:variable name="time">
				                        <xsl:value-of select="substring($DateTime,12,8)" />
				        </xsl:variable>
				        <xsl:variable name="hh">
				                        <xsl:value-of select="substring($time,1,2)" />
				        </xsl:variable>
				        <xsl:variable name="mm">
				                        <xsl:value-of select="substring($time,4,2)" />
				        </xsl:variable>
				        <xsl:variable name="ss">
				                        <xsl:value-of select="substring($time,7,2)" />
				        </xsl:variable>
				                <xsl:value-of select="$day"/>
				              	 <xsl:value-of select="'.'"/>
				                <xsl:value-of select="$mo"/>
				               <xsl:value-of select="'.'"/>
				                <xsl:value-of select="$year"/>
				                <xsl:value-of select="' '"/>
				                <xsl:value-of select="$hh"/>
				               <xsl:value-of select="':'"/>
				                <xsl:value-of select="$mm"/>
				                <xsl:value-of select="':'"/>				
				                <xsl:value-of select="$ss"/>
				        </xsl:template>
				
				
</xsl:stylesheet>

