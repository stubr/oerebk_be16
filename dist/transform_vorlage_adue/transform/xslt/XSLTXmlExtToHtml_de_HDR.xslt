<?xml version="1.0" encoding="utf-8"?>
<!--

 ***************************************************************
 	XSLT Stylesheeet fuer Header ÖREB

 	Version 0.1 (18.09.2013)
 	das.zeichen (Bernhard Sturm)

  ***************************************************************

 HISTORY
 *******
 v0.1 18.09.2013	Erste Version
 
-->



<xsl:stylesheet version="1.0" exclude-result-prefixes="msxsl"  
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:msxsl="urn:schemas-microsoft-com:xslt" 
	
>
				<xsl:output method="text" indent="no"/>
				<xsl:strip-space elements="*"/><!-- ROOT NODE -->
				

				
				<xsl:template match="Extraction"><!-- Header und Body -->
				
				<!-- Wichtige Variabeln -->
				<xsl:variable name="Kanton" select="//Kanton" />
				<xsl:variable name="Gemeinde" select="//Gemeinde" />
				<xsl:variable name="GemeindeNr" select="//Bfs" />
				<xsl:variable name="DateTimeCreated" select="//Erstellungsdatum" />
				<xsl:variable name="DateTimeCreatedFormat">
					<xsl:choose>
						<xsl:when test="$DateTimeCreated !='0000-00-00T00:00:00'">
							                        <xsl:call-template name="FormatDate">
								                                <xsl:with-param name="DateTime" select="$DateTimeCreated"/>
								           </xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text></xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>
				
				
<!-- 
				************************************************
******************************************
				
Inline Styles für den Header, immer die aktuellste Version aus oereb.css kopieren!
				******************************************************************************************
				-->
				<![CDATA[

				<style type="text/css">
				
				.header {
				  position: absolute;
				  top: 0.5cm;
				  margin-left: 0cm;
				  font-family: Arial, Helvetica, sans-serif;
				  font-size: 1em;
				}
				.header .logo {
				  height: 2.3cm;
				  position: absolute;
				  top: 0cm;
				  padding-left: 0.3cm;
				}
				.header .logo img {
				  height: 2cm;
				}
				.header .logo.kanton,
				.header .logo.gemeinde {
				  left: 11cm;
				  border-left: 0.05cm solid #000000;
				  padding-top: 0.5cm;
				}
				.header .logo.kanton img,
				.header .logo.gemeinde img {
				  height: 1.2cm;
				}
				.header .logo.gemeinde {
				  left: 16.4cm;
				  width: 2.6cm;
				}
				.header .logo.gemeinde p {
				  font-size: 0.6em;
				  line-height: 100%;
				  margin-top: 0.1cm;
				}
				
				</style>
				
]]>

			
				<!-- START HEADER -->
				<xsl:call-template name="template_header" />
				<!-- ENDE HEADER -->
						
						
			</xsl:template><!-- ENDE ROOT NODE -->
				
				<!-- DEFINTION DER TEMPLATES -->
				<!-- START FUSSZEILE -->
				<xsl:variable name="DateTimeCreated" select="//Erstellungsdatum" />
				<xsl:variable name="DateTimeCreatedFormat">
					<xsl:choose>
						<xsl:when test="$DateTimeCreated !='0000-00-00T00:00:00'">
						
							                        <xsl:call-template name="FormatDate">
								                                <xsl:with-param name="DateTime" select="$DateTimeCreated"/>
								           </xsl:call-template>

						</xsl:when>
						<xsl:otherwise>
							<xsl:text></xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>

				
				
				
				<xsl:template name="template_footer">
				
					<div class="footer" >
						<span class="footer-date-label">Auszug erstellt am:</span>
						<span class="footer-date-value"><xsl:value-of select="$DateTimeCreatedFormat" /></span>
						<span class="footer-id-label">ID-Nr.:</span>
						<span class="footer-id-value"><xsl:value-of select="//Id" /></span>
						<span class="footer-page-label">Seite</span>
						
						<span class="footer-page-value">{page_number} von {total_pages}</span>
					</div>
				
				</xsl:template>
				<!-- ENDE FUSSZEILE -->
				
				
				<!-- START HEADER -->
				<xsl:template name="template_header">
					<!-- Wichtige Variabeln -->

					<xsl:variable name="Kanton" select="//Kanton" />
					<xsl:variable name="Gemeinde" select="//Gemeinde" />
					<xsl:variable name="GemeindeNr" select="//Bfs" />

				<![CDATA[

					<div class="header">
						<div class="logo eidgenossenschaft">
							<img src="http://files.be.ch/bve/agi/oereb/logos/eidgenossenschaft.png" />	
						</div>		
					<div class="logo kanton">
					<img src="]]><xsl:value-of select="concat('http://files.be.ch/bve/agi/oereb/logos/kanton_',$Kanton,'.gif')" /><![CDATA[" />]]>
					<![CDATA[
						</div>
						<div class="logo gemeinde">
							<img src="]]><xsl:value-of select="concat('http://files.be.ch/bve/agi/oereb/logos/',$GemeindeNr,'.png')" /><![CDATA[" />
							<p>]]><xsl:value-of select="$Gemeinde"/><![CDATA[</p>
						</div>		
					</div>
					]]>
					
				</xsl:template>
				<!-- ENDE HEADER -->

				
				
	
				<!-- Template für Lower / Uppercase -->
				 <xsl:template name="ToLower">
				                <xsl:param name="inputString"/>
				                <xsl:variable name="smallCase" select="'abcdefghijklmnopqrstuvwxyzäöü'"/>
				                <xsl:variable name="upperCase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ'"/>
				                <xsl:value-of select="translate($inputString,$upperCase,$smallCase)"/>
				 </xsl:template>
				 
				 <xsl:template name="ToUpper">
				                <xsl:param name="inputString"/>
				                <xsl:variable name="smallCase" select="'abcdefghijklmnopqrstuvwxyzäöü'"/>
				                <xsl:variable name="upperCase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ'"/>
				            
				                <xsl:value-of select="translate($inputString,$smallCase,$upperCase)"/>
				 
				   </xsl:template>
				
				
<!-- DATUMSFORMATE -->
				
        <xsl:template name="FormatDate">
				        <xsl:param name="DateTime" />
				        <xsl:variable name="mo">
				                        <xsl:value-of select="substring($DateTime,6,2)" />
				        </xsl:variable>
				        <xsl:variable name="day">
				                        <xsl:value-of select="substring($DateTime,9,2)" />
				          </xsl:variable>
				        <xsl:variable name="year">
				                        <xsl:value-of select="substring($DateTime,1,4)" />
				        </xsl:variable>
				        <xsl:variable name="time">
				                        <xsl:value-of select="substring($DateTime,12,8)" />
				        </xsl:variable>
				        <xsl:variable name="hh">
				                        <xsl:value-of select="substring($time,1,2)" />
				        </xsl:variable>
				        <xsl:variable name="mm">
				                        <xsl:value-of select="substring($time,4,2)" />
				        </xsl:variable>
				        <xsl:variable name="ss">
				                        <xsl:value-of select="substring($time,7,2)" />
				        </xsl:variable>
				                <xsl:value-of select="$day"/>
				              	 <xsl:value-of select="'.'"/>
				                <xsl:value-of select="$mo"/>
				               <xsl:value-of select="'.'"/>
				                <xsl:value-of select="$year"/>
				                <xsl:value-of select="' '"/>
				                <xsl:value-of select="$hh"/>
				               <xsl:value-of select="':'"/>
				                <xsl:value-of select="$mm"/>
				                <xsl:value-of select="':'"/>				
				                <xsl:value-of select="$ss"/>
				        </xsl:template>
				
				
</xsl:stylesheet>
