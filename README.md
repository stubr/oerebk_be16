# README #

Dieses Repository beinhaltet alle notwendigen Dateien für die statischen OEREB-Kataster-Auszüge des Kantons Bern.

### Für was ist dieses Repository? ###

* Installation und Entwicklung
* Versioninierung
* Backup

### Welche Dateien benötige ich? ###

* Ich betreibe den Webservice:
Dann benötigen Sie nur gerade das Verzeichnis unter

```
#!html
/dist/oereb_transform/
```

* Ich richte das Geoportal ein (chronoforms mit Joomla)
Die chronoforms-Files sind alle unter 

```
#!html
web/chronoforms/oereb_forms /
```

### Wer hat was gemacht? ###

* Owner des Repos: [bs@sturmundbream.ch](mailto:bs@sturmundbraem.ch) (Bernhard Sturm)