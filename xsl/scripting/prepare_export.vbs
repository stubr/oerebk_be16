' Deleting Files


Const DeleteReadOnly = True

' Path to current directory... this may change depending on your set up
WorkingDirectory = "D:\sturmundbraem\stubr\projekte\agi-gis\oerebk_be16\dev\dev.bs.nova\"

Set objFSO = CreateObject("Scripting.FileSystemObject")
' Deleting XSLTs
objFSO.DeleteFile(WorkingDirectory & "dist\oereb_transform\xslt\*.xslt"), DeleteReadOnly
' Deleting css
objFSO.DeleteFile(WorkingDirectory & "dist\oereb_transform\css\*.css"), DeleteReadOnly

' Copy Files
Const OverwriteExisting = True
Set objFSO = CreateObject("Scripting.FileSystemObject")
' Deutsche Version CSS
objFSO.CopyFile WorkingDirectory & "xsl\html\oereb.css" , WorkingDirectory & "dist\oereb_transform\css\oereb_de.css", OverwriteExisting
'Französische Version CSS
objFSO.CopyFile WorkingDirectory & "xsl\html\oereb.css" , WorkingDirectory & "dist\oereb_transform\css\oereb_fr.css", OverwriteExisting

' Copy all XSLTs
' Language Files
objFSO.CopyFile WorkingDirectory & "xsl\xslt\language_de.xslt" , WorkingDirectory & "dist\oereb_transform\xslt\language_de.xslt", OverwriteExisting
objFSO.CopyFile WorkingDirectory & "xsl\xslt\language_fr.xslt" , WorkingDirectory & "dist\oereb_transform\xslt\language_fr.xslt", OverwriteExisting

' 1:1 XSLTs
objFSO.CopyFile WorkingDirectory & "xsl\xslt\XSLTXmlIntToXmlExt_de.xslt" , WorkingDirectory & "dist\oereb_transform\xslt\XSLTXmlIntToXmlExt_de.xslt", OverwriteExisting
objFSO.CopyFile WorkingDirectory & "xsl\xslt\XSLTXmlIntToXmlExt_fr.xslt" , WorkingDirectory & "dist\oereb_transform\xslt\XSLTXmlIntToXmlExt_fr.xslt", OverwriteExisting
' 1:1 XML.CH
objFSO.CopyFile WorkingDirectory & "xsl\xslt\XSLTXmlChIntToXmlChExt_de.xslt" , WorkingDirectory & "dist\oereb_transform\xslt\XSLTXmlChIntToXmlChExt_de.xslt", OverwriteExisting
objFSO.CopyFile WorkingDirectory & "xsl\xslt\XSLTXmlChIntToXmlChExt_fr.xslt" , WorkingDirectory & "dist\oereb_transform\xslt\XSLTXmlChIntToXmlChExt_fr.xslt", OverwriteExisting

' Root XSLTs
objFSO.CopyFile WorkingDirectory & "xsl\xslt\XSLTXmlExtToHtml.xslt" , WorkingDirectory & "dist\oereb_transform\xslt\XSLTXmlExtToHtml_de.xslt", OverwriteExisting
objFSO.CopyFile WorkingDirectory & "xsl\xslt\XSLTXmlExtToHtml.xslt" , WorkingDirectory & "dist\oereb_transform\xslt\XSLTXmlExtToHtml_fr.xslt", OverwriteExisting

' Header XSLTs
objFSO.CopyFile WorkingDirectory & "xsl\xslt\XSLTXmlExtToHeader_de.xslt" , WorkingDirectory & "dist\oereb_transform\xslt\XSLTXmlExtToHtml_de_HDR.xslt", OverwriteExisting
objFSO.CopyFile WorkingDirectory & "xsl\xslt\XSLTXmlExtToHeader_fr.xslt" , WorkingDirectory & "dist\oereb_transform\xslt\XSLTXmlExtToHtml_fr_HDR.xslt", OverwriteExisting
' Footer XSLTs
objFSO.CopyFile WorkingDirectory & "xsl\xslt\XSLTXmlExtToFooter_de.xslt" , WorkingDirectory & "dist\oereb_transform\xslt\XSLTXmlExtToHtml_de_FTR.xslt", OverwriteExisting
objFSO.CopyFile WorkingDirectory & "xsl\xslt\XSLTXmlExtToFooter_fr.xslt" , WorkingDirectory & "dist\oereb_transform\xslt\XSLTXmlExtToHtml_fr_FTR.xslt", OverwriteExisting


' Modifiy XSLT
' Replace Reference language_de in _fr XSLT
Const ForReading = 1
Const ForWriting = 2

Set objFS = CreateObject("Scripting.FileSystemObject")
strFile = WorkingDirectory & "dist\oereb_transform\xslt\XSLTXmlExtToHtml_fr.xslt"
Set objFile = objFS.OpenTextFile(strFile, ForReading)

strText=objFile.ReadAll
objFile.close
strNewText=Replace(strText,"language_de.xslt","language_fr.xslt")

Set objFile = objFSO.OpenTextFile(strFile, ForWriting)
objFile.WriteLine strNewText
objFile.Close

Wscript.echo "Fertig"
