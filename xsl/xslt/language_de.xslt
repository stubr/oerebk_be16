<?xml version="1.0" encoding="utf-8"?>
<!--

 ***************************************************************
 	XSLT Stylesheeet fuer ÖREB-Kataster Auszüge
  Sprachversion DEUTSCH

 	Version siehe Bitbucket (https://bitbucket.org/stubr/oerebk_be16)
 	STURM UND BRÄM 2016

  ***************************************************************

-->


<xsl:stylesheet version="1.0" exclude-result-prefixes="msxsl" extension-element-prefixes="date" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:msxsl="urn:schemas-microsoft-com:xslt" xmlns:date="http://exslt.org/dates-and-times">

<!-- Hier folgen alle Sprachvariabeln auf DEUTSCH -->
<xsl:variable name="DokumentAutor">Kanton Bern, Amt für Geoinformation</xsl:variable>
<xsl:variable name="TextTitel">Auszug aus dem Kataster der öffentlich-rechtlichen Eigentumsbeschränkungen (ÖREB-Kataster)</xsl:variable>
<xsl:variable name="TextLiegenschaftNr">Grundstück-Nr.</xsl:variable>
<xsl:variable name="TextGemeinde">Gemeinde</xsl:variable>
<xsl:variable name="TextEGRIDNr">E-GRID</xsl:variable>
<xsl:variable name="TextLiegenschaftsflaeche">Fläche</xsl:variable>
<xsl:variable name="TextBFS">BFS-Nr.</xsl:variable>
<xsl:variable name="TextDatumAuszug">Erstellungsdatum des Auszugs</xsl:variable>
<xsl:variable name="TextErstellerAuszug">Katasterverantwortliche Stelle</xsl:variable>
<xsl:variable name="TextUnterschrift">Unterschrift</xsl:variable>
<xsl:variable name="TextInhaltsverzeichnis">Übersicht ÖREB-Themen</xsl:variable>
<xsl:variable name="TextSeite">Seite</xsl:variable>
<xsl:variable name="TextAnhaenge">Anhänge</xsl:variable>
<xsl:variable name="TextZustaendigeStelle">Zuständige Stelle</xsl:variable>
<xsl:variable name="TextGlossar">Glossar/Abkürzungen</xsl:variable>
<xsl:variable name="TextSignatur">Unterschrift</xsl:variable>
<xsl:variable name="TextIDNr">Auszugsnummer</xsl:variable>
<xsl:variable name="TextTocKeineBeschraenkung">Öffentlich-rechtliche Eigentumsbeschränkungen, welche das Grundstück nicht betreffen</xsl:variable>
<xsl:variable name="TextTocBeschraenkung">Öffentlich-rechtliche Eigentumsbeschränkungen, welche das Grundstück</xsl:variable>
<xsl:variable name="TextTocBeschraenkungNichtVerfuegbar">Öffentlich-rechtliche Eigentumsbeschränkungen, zu denen noch keine Daten vorhanden sind</xsl:variable>
<xsl:variable name="TextKreisBez">Grundbuchkreis</xsl:variable>
<xsl:variable name="TextNorthArrow">Norden</xsl:variable>
<xsl:variable name="TextTitelBeglaubigung">Beglaubigung</xsl:variable>
<xsl:variable name="TextBeglaubigungArtikel">Gemäss EV ÖREBKV Art.3</xsl:variable>
<xsl:variable name="TextBeglaubigungURL">https://www.belex.sites.be.ch/frontend/texts_of_law/247</xsl:variable>
<xsl:variable name="TextStempel">Stempel</xsl:variable>
<xsl:variable name="TextDatum">Datum</xsl:variable>
<xsl:variable name="TextTocIn"> in </xsl:variable>
<xsl:variable name="TextTocBetreffen"> betreffen</xsl:variable>
<xsl:variable name="TextLegendeBeteiligtObjekt">Legende der betroffenen ÖREB</xsl:variable>
<xsl:variable name="TextLegendeSichtbar">Übrige Legende (im sichtbaren Bereich)</xsl:variable>
<xsl:variable name="TextLegendeVollstaendig">Vollständige Legende</xsl:variable>
<xsl:variable name="TextTitelAnteil">Anteil</xsl:variable>
<xsl:variable name="TextTitelFlaeche">Fläche</xsl:variable>
<xsl:variable name="TextTitelAnzahl">Anzahl</xsl:variable>
<xsl:variable name="TextTitelLaenge">Länge</xsl:variable>
<xsl:variable name="TextTitelTyp">Typ</xsl:variable>
<xsl:variable name="TextTitelGeneralInformation">Allgemeine Informationen</xsl:variable>
<xsl:variable name="TextTitelBaseData">Grundlagendaten</xsl:variable>

</xsl:stylesheet>
