<?xml version="1.0" encoding="utf-8"?>
<!--

 ***************************************************************
 	XSLT Stylesheeet fuer ÖREB-Kataster Auszüge

	Version siehe Bitbucket (https://bitbucket.org/stubr/oerebk_be16)
 	STURM UND BRÄM 2016 - 18

  ***************************************************************

-->



<xsl:stylesheet version="1.0" exclude-result-prefixes="msxsl"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:msxsl="urn:schemas-microsoft-com:xslt"
>
				<xsl:decimal-format name="swiss" decimal-separator='.' grouping-separator="'" />
				<xsl:output method="html" indent="yes"/>
				<xsl:strip-space elements="*"/><!-- ROOT NODE -->

				<!-- Als erstes ersetzen werden alle leeren Werte mit einem - gefüllt -->
				<xsl:variable name="EmptyValue">-</xsl:variable>



				<!-- Include Sprachvariabeln (abhängig der Sprachversion
				A C H T U N G ! ! Dieser Wert muss manuell gesetzt werden
				-->
				<xsl:include href="language_de.xslt" />


				<xsl:template match="Extraction"><!-- Header und Body -->

				<!-- Wichtige Variabeln -->
				<xsl:variable name="Kanton" select="//Kanton" />
				<xsl:variable name="GrundstueckNr" select="//Parcel/Nummer" />
				<xsl:variable name="Gemeinde" select="//Gemeinde" />
				<xsl:variable name="GemeindeNr" select="//Bfs" />
				<xsl:variable name="KreisBez" select="//KreisBez" />
				<xsl:variable name="Reduziert" select="//IstReduziert" />
				<xsl:variable name="DateTimeCreated" select="//Erstellungsdatum" />
				<xsl:variable name="DateTimeCreatedFormat">
					<xsl:choose>
						<xsl:when test="$DateTimeCreated !='0000-00-00T00:00:00'">
							<xsl:call-template name="FormatDateShort">
								<xsl:with-param name="DateTime" select="$DateTimeCreated"/>
							</xsl:call-template>
						</xsl:when>
						<xsl:otherwise>
							<xsl:text></xsl:text>
						</xsl:otherwise>
					</xsl:choose>
				</xsl:variable>



				<html>
					<head>
						<meta name="author" content="{$DokumentAutor}"/>
						<meta http-equiv="Pragma" content="no-cache"/>
						<meta http-equiv="Expires" content="-1"/>

						<link rel="stylesheet" type="text/css" href="oereb.css"/>

					</head>
					<body>

						<!-- START DECKBLATT -->
						<div class="page deckblatt">
				 			<h1><xsl:value-of select="$TextTitel"/></h1>
							<div class="map">
								<img>
									<xsl:attribute name="id">front-wms</xsl:attribute>
									<xsl:attribute name="src"><xsl:value-of select="//Parcel/VerweisWMS" /></xsl:attribute>
									<xsl:attribute name="alt"></xsl:attribute>
								</img>

								<xsl:choose>
									<!-- Gibt es ein REST-Bild? -->
									<xsl:when test="//Parcel/HasVerweisREST='true'">
							 			<img>
											<xsl:attribute name="id">front-rest</xsl:attribute>
											<xsl:attribute name="src"><xsl:value-of select="//Parcel/VerweisREST" /></xsl:attribute>
											<xsl:attribute name="alt"></xsl:attribute>

										</img>
									</xsl:when>
									<xsl:otherwise>
									</xsl:otherwise>
								</xsl:choose>



								<!-- SCALE BAR -->

								<xsl:variable name="ScaleBarWidthMax">50</xsl:variable>
								<xsl:variable name="ScaleBarMiddle" select="//Measures/ScaleBarMeasure/MMeter" />

								<xsl:variable name="ScaleBarMiddleMeter" select="//Measures/ScaleBarMeasure/Meter" />

								<xsl:variable name="ScaleBarRight" select="//Measures/ScaleBarMeasure[2]/MMeter" />

								<xsl:variable name="ScaleBarRightMeter" select="//Measures/ScaleBarMeasure[2]/Meter" />

								<xsl:variable name="ScaleBarOffset" select="($ScaleBarWidthMax div 2) - $ScaleBarMiddle" />


								<div class="mapscale" >
									<div class="northarrow">
										<img>
											<xsl:attribute name="src"><xsl:value-of select="//Parcel/ScaleBarInfo/MapNorthArrowSymbol" /></xsl:attribute>
											<xsl:attribute name="alt"><xsl:value-of select="$TextNorthArrow" /></xsl:attribute>
										</img>
									</div>
									<div class="scaleindicator" >
									 	<div>
									 		<xsl:attribute name="class">scalebackground</xsl:attribute>
									 		<xsl:attribute name="style">width:<xsl:value-of select="$ScaleBarRight + 2"/>mm;left:<xsl:value-of select="$ScaleBarOffset - 1"/>mm;</xsl:attribute>
									 	</div>
										<div>
											<xsl:attribute name="class">bar-top-left</xsl:attribute>
											<xsl:attribute name="style">width:<xsl:value-of select="$ScaleBarMiddle"/>mm;left:<xsl:value-of select="$ScaleBarOffset"/>mm;</xsl:attribute>
										</div>
										<div>
											<xsl:attribute name="class">bar-top-right</xsl:attribute>
											<xsl:attribute name="style">width:<xsl:value-of select="$ScaleBarRight - $ScaleBarMiddle"/>mm;left:<xsl:value-of select="$ScaleBarOffset + $ScaleBarMiddle"/>mm;</xsl:attribute>
										</div>
										<div>
											<xsl:attribute name="class">bar-bottom-left</xsl:attribute>
											<xsl:attribute name="style">width:<xsl:value-of select="$ScaleBarMiddle"/>mm;left:<xsl:value-of select="$ScaleBarOffset"/>mm;</xsl:attribute>
										</div>
										<div>
											<xsl:attribute name="class">bar-bottom-right</xsl:attribute>
											<xsl:attribute name="style">width:<xsl:value-of select="$ScaleBarRight - $ScaleBarMiddle"/>mm;left:<xsl:value-of select="$ScaleBarOffset + $ScaleBarMiddle"/>mm;</xsl:attribute>
										</div>
										<div>
											<xsl:attribute name="class">scale-marker-left</xsl:attribute>
											<xsl:attribute name="style">left:<xsl:value-of select="$ScaleBarOffset - 2" />mm;</xsl:attribute>
											<xsl:text>0</xsl:text>
										</div>
										<div>
											<xsl:attribute name="class">scale-marker-middle</xsl:attribute>
											<xsl:value-of select="$ScaleBarMiddleMeter"/>
										</div>
										<div>
											<xsl:attribute name="class">scale-marker-right</xsl:attribute>
											<xsl:attribute name="style">right: <xsl:value-of select="$ScaleBarOffset - 5" />mm;</xsl:attribute>
											<xsl:value-of select="$ScaleBarRightMeter"/>m
										</div>

									</div>
								</div>

							</div>
							<div class="info">
								<div class="row cols clearfix strong">
									<div class="col-1">
										<span class="label"><xsl:value-of select="$TextLiegenschaftNr" /></span>
										<span class="value"><xsl:value-of select="$GrundstueckNr" /></span>
									</div>
								</div>
								<div class="row cols clearfix" >
									<div class="col-1">
										<span class="label"><xsl:value-of select="$TextEGRIDNr" /></span>
										<span class="value"><xsl:value-of select="//Egrid" /></span>
									</div>
								</div>
								<div class="row cols clearfix">
							  		<div class="col-1">
										<span class="label"><xsl:value-of select="$TextGemeinde" /> (<xsl:value-of select="$TextBFS" />)</span>
										<span class="value"><xsl:value-of select="$Gemeinde"/> (<xsl:value-of select="//Bfs" />)</span>
									</div>
								</div>
								<div class="row cols clearfix" >
									<div class="col-1">
										<span class="label"><xsl:value-of select="$TextKreisBez" /></span>
										<span class="value"><xsl:value-of select="//KreisBez" /></span>
									</div>
								</div>
								<div class="row cols clearfix">
									<div class="col-1">
										<span class="label"><xsl:value-of select="$TextLiegenschaftsflaeche" /></span>
										<span class="value"><xsl:value-of select='format-number(//GbFlaeche, "###&apos;###.##","swiss")' />m<span class="upper">2</span></span>
									</div>
								</div>
								<div class="row cols clearfix strong">
									<div class="col-1">
										<span class="label"><xsl:value-of select="$TextIDNr" /></span>
										<span class="value"><xsl:value-of select="//ExtHeader/Info/Id" /></span>
									</div>
								</div>


								<div class="row cols clearfix">
									<div class="col-1">
										<span class="label"><xsl:value-of select="$TextDatumAuszug" /></span>
										<span class="value"><xsl:value-of select="$DateTimeCreatedFormat" /></span>
									</div>
								</div>
								<div class="row cols clearfix">
							  		<div class="col-1">
										<span class="label"><xsl:value-of select="$TextErstellerAuszug" /></span>
										<span class="value"><xsl:value-of select="//Cadaster/Name" />, <xsl:value-of select="//Cadaster/Street" /><xsl:text> </xsl:text><xsl:value-of select="//Cadaster/Number" />,<xsl:text> </xsl:text><xsl:value-of select="//Cadaster/Postalcode" /><xsl:text> </xsl:text><xsl:value-of select="//Cadaster/City" /></span>
									</div>
								</div>
							</div>
							<div class="beglaubigung">
								<p><strong><xsl:value-of select="$TextTitelBeglaubigung"/></strong></p>
								<p><xsl:value-of select="$TextBeglaubigungArtikel"/>,<xsl:text> </xsl:text>
								<a>
									<xsl:attribute name="class">url-link</xsl:attribute>
									<xsl:attribute name="href"><xsl:value-of select="$TextBeglaubigungURL"/></xsl:attribute>
									<xsl:value-of select="$TextBeglaubigungURL"/>
								</a>
								</p>
							</div>
							<div class="signature">
								<span class="label-1">(<xsl:value-of select="$TextStempel" />)</span>
								<span class="label-2">(<xsl:value-of select="$TextDatum" />)</span>
								<span class="label-3">(<xsl:value-of select="$TextSignatur" />)</span>

							</div>

						</div>
						<!-- ENDE DECKBLAT -->

						 <!-- START INHALTSVERZEICHNIS -->
						 <div class="page toc">
						 	<h2><xsl:value-of select="$TextInhaltsverzeichnis" /></h2>
						 	<div class="toc-info">
						 	<h3><xsl:value-of select="$TextTocBeschraenkung" /><xsl:text> </xsl:text><xsl:value-of select="$GrundstueckNr"/><xsl:value-of select="$TextTocIn" /><xsl:value-of select="$Gemeinde"/>
							<xsl:if test="$KreisBez != ''"><xsl:text>, </xsl:text><xsl:value-of select="$KreisBez" />
							</xsl:if>
							<xsl:value-of select="$TextTocBetreffen" /></h3>

							<table class="toc-table">
							<tr>
								<td class="tcol1 colspan1-2" colspan="2" ><xsl:value-of select="$TextSeite"/></td>
							</tr>




							<xsl:for-each select="//TocThemesWithIntersect/TocTheme">
							<tr>
								<td class="definition tcol1"><xsl:value-of select="ThemePageNumber" /></td>
								<td class="value tcol2"><xsl:value-of select="Name"/></td>
							</tr>
							</xsl:for-each>
							</table>
						               <h3><xsl:value-of select="$TextTocKeineBeschraenkung" /> </h3>
						               <xsl:for-each select="//TocThemesWithoutIntersect/TocTheme">
								<p><xsl:value-of select="Name"/></p>
							</xsl:for-each>
						               <h3><xsl:value-of select="$TextTocBeschraenkungNichtVerfuegbar" /> </h3>
						               <xsl:for-each select="//TocThemesNotAvailable/TocTheme">
							<p><xsl:value-of select="Name"/></p>
							</xsl:for-each>

							<div class="row reservation" id="hinweise" >
								<h3 id="generalinformation"><xsl:value-of select="$TextTitelGeneralInformation" /></h3>
								<p><xsl:value-of select="//GeneralInformation" /></p>
								<h3 id="basedata"><xsl:value-of select="$TextTitelBaseData" /></h3>
								<p id="p_basedata"><xsl:value-of select="//BaseData" /></p>
								<xsl:for-each select="//Reservation">
								<h3><xsl:value-of select="Titel" /></h3>
								<p><xsl:value-of select="Vorbehalt"/> </p>
								</xsl:for-each>
							</div>

							</div>

						 </div>



						<!-- START BESCHRÄNKUNGEN -->
						<!-- Durch alle Subthemes loopen -->
						<xsl:for-each select="//SubThemes/SubThemeInfo">

						<!-- Ist das eine zutreffende Restriction? -->

						<xsl:if test="HasRestrictions='true'">

							<!-- ENDE INHALTSVERZEICHNIS -->

							<!-- EINE RESTRICTION PRO SEITE -->
							<div class="page restrictions" >
									<h1 class="subThemeTitle" ><xsl:value-of select="SubTheme/NameTheme"/>
									<xsl:choose>
										<xsl:when test="SubTheme/NameTheme != SubTheme/NameSubTheme">
											<xsl:text>: </xsl:text><br />
											<xsl:value-of select="SubTheme/NameSubTheme" />
										</xsl:when>
									</xsl:choose>
									</h1>
									<div class="map">

										<img>
											<xsl:attribute name="class">wms</xsl:attribute>
											<xsl:attribute name="src"><xsl:value-of select=".//PropertyRestriction/VerweisWMS" /></xsl:attribute>
											<xsl:attribute name="alt"></xsl:attribute>
										</img>
										<img>
											<xsl:attribute name="class">situation</xsl:attribute>
											<xsl:attribute name="src"><xsl:value-of select=".//PropertyRestriction/VerweisWMSParcel" /></xsl:attribute>
											<xsl:attribute name="alt"></xsl:attribute>
										</img>


										<xsl:choose>
											<!-- Gibt es ein REST-Bild? -->
											<xsl:when test=".//PropertyRestriction/HasVerweisREST='true'">
												<img>
													<xsl:attribute name="class">restriction</xsl:attribute>
													<xsl:attribute name="src"><xsl:value-of select=".//PropertyRestriction/VerweisREST" /></xsl:attribute>
													<xsl:attribute name="alt"></xsl:attribute>
												</img>
											</xsl:when>
											<xsl:otherwise>
											</xsl:otherwise>
										</xsl:choose>

									<!-- SCALE BAR -->

									<xsl:variable name="ScaleBarWidthMax">50</xsl:variable>
									<xsl:variable name="ScaleBarMiddle" select=".//PropertyRestriction/ScaleBarInfo/Measures/ScaleBarMeasure/MMeter" />

									<xsl:variable name="ScaleBarMiddleMeter" select=".//PropertyRestriction/ScaleBarInfo/Measures/ScaleBarMeasure/Meter" />

									<xsl:variable name="ScaleBarRight" select=".//PropertyRestriction/ScaleBarInfo/Measures/ScaleBarMeasure[2]/MMeter" />

									<xsl:variable name="ScaleBarRightMeter" select=".//PropertyRestriction/ScaleBarInfo/Measures/ScaleBarMeasure[2]/Meter" />

									<xsl:variable name="ScaleBarOffset" select="($ScaleBarWidthMax div 2) - $ScaleBarMiddle" />


									<div class="mapscale" >
										<div class="northarrow">
											<img>
												<xsl:attribute name="src"><xsl:value-of select=".//PropertyRestriction/ScaleBarInfo/MapNorthArrowSymbol" /></xsl:attribute>
												<xsl:attribute name="alt"><xsl:value-of select="$TextNorthArrow" /></xsl:attribute>
											</img>
										</div>
										<div class="scaleindicator" >
										 	<div>
										 		<xsl:attribute name="class">scalebackground</xsl:attribute>
										 		<xsl:attribute name="style">width:<xsl:value-of select="$ScaleBarRight + 1.6"/>mm;left:<xsl:value-of select="$ScaleBarOffset - 0.6"/>mm;</xsl:attribute>
										 	</div>
											<div>
												<xsl:attribute name="class">bar-top-left</xsl:attribute>
												<xsl:attribute name="style">width:<xsl:value-of select="$ScaleBarMiddle"/>mm;left:<xsl:value-of select="$ScaleBarOffset"/>mm;</xsl:attribute>
											</div>
											<div>
												<xsl:attribute name="class">bar-top-right</xsl:attribute>
												<xsl:attribute name="style">width:<xsl:value-of select="$ScaleBarRight - $ScaleBarMiddle"/>mm;left:<xsl:value-of select="$ScaleBarOffset + $ScaleBarMiddle"/>mm;</xsl:attribute>
											</div>
											<div>
												<xsl:attribute name="class">bar-bottom-left</xsl:attribute>
												<xsl:attribute name="style">width:<xsl:value-of select="$ScaleBarMiddle"/>mm;left:<xsl:value-of select="$ScaleBarOffset"/>mm;</xsl:attribute>
											</div>
											<div>
												<xsl:attribute name="class">bar-bottom-right</xsl:attribute>
												<xsl:attribute name="style">width:<xsl:value-of select="$ScaleBarRight - $ScaleBarMiddle"/>mm;left:<xsl:value-of select="$ScaleBarOffset + $ScaleBarMiddle"/>mm;</xsl:attribute>
											</div>
											<div>
												<xsl:attribute name="class">scale-marker-left</xsl:attribute>
												<xsl:attribute name="style">left:<xsl:value-of select="$ScaleBarOffset - 2" />mm;</xsl:attribute>
												<xsl:text>0</xsl:text>
											</div>
											<div>
												<xsl:attribute name="class">scale-marker-middle</xsl:attribute>
												<xsl:value-of select="$ScaleBarMiddleMeter"/>
											</div>
											<div>
												<xsl:attribute name="class">scale-marker-right</xsl:attribute>
												<xsl:attribute name="style">right: <xsl:value-of select="$ScaleBarOffset - 5" />mm;</xsl:attribute>
												<xsl:value-of select="$ScaleBarRightMeter"/>m
											</div>

										</div>
									</div>
									</div>
									<div class="info">

					<!-- VARIABEL -->

					<xsl:variable name="GeometrieQuelle"><xsl:value-of select=".//Aggregation/ConcernedObjects/LegendObjects/GeometrySource"/></xsl:variable>
										<table class="property-info">
											<tr class="header">
												<td class="col5span1-2" colspan="2"><xsl:text> </xsl:text></td>
												<td class="type"><xsl:value-of select="$TextTitelTyp" /></td>
												<td class="area">
												<xsl:choose>
													<xsl:when test="$GeometrieQuelle = 'Polygon'">
														<xsl:value-of select="$TextTitelFlaeche" />
													</xsl:when>
													<xsl:when test="$GeometrieQuelle = 'Polyline'">
														<xsl:value-of select="$TextTitelLaenge" />
													</xsl:when>
													<xsl:when test="$GeometrieQuelle = 'Point'">
														<xsl:value-of select="$TextTitelAnzahl" />
													</xsl:when>

												</xsl:choose>
												</td>
												<td class="percentage"><xsl:value-of select="$TextTitelAnteil" /></td>
											</tr>
											<tr class="body">
												<td class="label strong"><xsl:value-of select="$TextLegendeBeteiligtObjekt" /></td>
												<td class="col5span2-5" colspan="4">
												<!-- Durch alle Legenden-Bilder loopen -->
													<table class="key-table col5span2-5">
													<xsl:for-each select=".//Aggregation/ConcernedObjects//Legend">
														<tr>
															<td class="key">
																<img>
																	<xsl:attribute name="class">legend-image</xsl:attribute>
																	<xsl:attribute name="src"><xsl:value-of select="LegendSymbol"/></xsl:attribute>
																</img>
															</td>
															<td class="type" ><xsl:value-of select="Name"/></td>
															<xsl:choose>
																<xsl:when test="$GeometrieQuelle = 'Polygon'">
																<td class="area" ><xsl:value-of select='format-number(Absolute, "###&apos;###.##","swiss")' />
																m<span class="upper">2</span></td>
																</xsl:when>
																<xsl:when test="$GeometrieQuelle = 'Polyline'">
																<td class="area" ><xsl:value-of select='format-number(Absolute, "###&apos;###.##","swiss")' />
																m</td>
																</xsl:when>
																<xsl:when test="$GeometrieQuelle = 'Point'">
																<td class="area" ><xsl:value-of select='format-number(Absolute, "###&apos;###.##","swiss")' /></td>
																</xsl:when>
															</xsl:choose>
															
															<xsl:choose>
																<xsl:when test="$GeometrieQuelle = 'Polygon'">
																	<td class="percentage" ><xsl:value-of select="Percentage"/>%</td>
																</xsl:when>
																<xsl:otherwise>
																	<td class="percentage" >-</td>
																</xsl:otherwise>
															</xsl:choose>
														</tr>
													</xsl:for-each>
													</table>

												</td>
											</tr>
										</table>
										<table class="property-info">
											<tr class="body">
												<td class="label strong"><xsl:value-of select="$TextLegendeSichtbar" /></td>
												<td class="col5span2-5" colspan="4">
												<!-- Durch alle Legenden-Bilder loopen -->
													<table class="key-table col5span2-5">
													<xsl:for-each select=".//Aggregation/OtherObjects//Legend">
														<tr>
															<td class="key">
																<img>
																	<xsl:attribute name="class">legend-image</xsl:attribute>
																	<xsl:attribute name="src"><xsl:value-of select="LegendSymbol"/></xsl:attribute>
																</img>
															</td>
															<td class="type" ><xsl:value-of select="Name"/></td>
															<td class="area" ><xsl:text> </xsl:text></td>
															<td class="percentage" ><xsl:text> </xsl:text></td>
														</tr>
													</xsl:for-each>
													</table>

												</td>
											</tr>
											<tr class="body">
												<td class="label strong"><xsl:value-of select="$TextLegendeVollstaendig" /></td>
												<td class="col5span2-5" colspan="4">
													<a>
														<xsl:attribute name="class">url-link</xsl:attribute>
														<xsl:attribute name="href"><xsl:value-of select=".//Aggregation/FullLegend"/></xsl:attribute>
														<xsl:value-of select=".//Aggregation/FullLegend"/>
													</a>

												</td>
											</tr>
										</table>
											<table class="property-info">
											<xsl:for-each select=".//Aggregation//TypedRegulationList/TypedRegulationsAgg">

											<tr class="body">
												<td class="label strong"><xsl:value-of select="TypeName" /></td>
												<td class="col5span2-5" colspan="4">
													<xsl:if test="HasRegulations = 'true'">
														<!-- Durch alle Regulations loopen -->
														<xsl:for-each select="RegulationList/RegulationAgg">
														<xsl:choose>
															<xsl:when test="OffiziellerTitel != ''">
																<xsl:value-of select="OffiziellerTitel"/>
															</xsl:when>
															<xsl:otherwise>
																<xsl:value-of select="Titel"/>
															</xsl:otherwise>
														</xsl:choose>
														<xsl:if test="TextImWeb != ''">
															<xsl:text>:</xsl:text>
														</xsl:if>
														<br />
														<a>
															<xsl:attribute name="class">url-link regulation-link</xsl:attribute>
															<xsl:attribute name="href"><xsl:value-of select="TextImWeb"/></xsl:attribute>
															<xsl:value-of select="TextImWeb"/>
														</a><br/>
														</xsl:for-each>
													</xsl:if>
													<xsl:if test="HasRegulations = 'false'">
														&#8212;
													</xsl:if>
												</td>
											</tr>

											</xsl:for-each>
											<tr class="body">
												<td class="label strong"><xsl:value-of select="$TextZustaendigeStelle" /></td>
												<td class="col5span2-5" colspan="4">
												<xsl:for-each select=".//Aggregation/Responsibles/Amt">
													<xsl:value-of select="Name"/>
													<xsl:if test="AmtImWeb !=''">
													<xsl:text>: </xsl:text>
													</xsl:if>
													<br />
													<a>
														<xsl:attribute name="class">url-link regulation-link</xsl:attribute>
														<xsl:attribute name="href"><xsl:value-of select="AmtImWeb"/></xsl:attribute>
														<xsl:value-of select="AmtImWeb"/>
													</a>
													<br />
												</xsl:for-each>
												</td>
											</tr>


										</table>






									</div>


							</div>
							</xsl:if>
							<!-- End If gültige Restriction -->

						</xsl:for-each>
						<!-- ENDE BESCHRÄNKUNGEN -->



						<!-- START GLOSSAR -->
						<xsl:if test="//HasGlossary = 'true'">

							<div class="page glossary" >
								<h2><xsl:value-of select="$TextGlossar" /></h2	>
								<div class="info" >
								<table class="property-info">
									<xsl:for-each select="//Glossary/GlossaryEntry">
									<tr class="body">
										<td class="label"><span class="strong"><xsl:value-of select="Title" />: </span><xsl:value-of select="Value" /></td>
									</tr>
									</xsl:for-each>
								</table>

								</div>
							</div>

						</xsl:if>

						<!-- END GLOSSAR -->





						<!-- START ANNEXE -->
						<!-- BEI BEGLAUBIGTEM AUSZUG, KEINE ANHÄNGE -->
						<xsl:choose>
							<xsl:when test="$Reduziert = 'true'"></xsl:when>

							<xsl:otherwise>

								<div class="page annexe" >
								<h2><xsl:value-of select="$TextAnhaenge" /></h2>
								<div class="info">
								<table class="property-info">
								<xsl:for-each select="//Annex">
									<tr class="body">
										<td class="value">
										<!-- <strong><xsl:value-of select="Title"/></strong></p>-->
										<a>
											<xsl:attribute name="target">_blank</xsl:attribute>
											<xsl:attribute name="href"><xsl:value-of select="Uri" /></xsl:attribute>
											<xsl:attribute name="class">url-link</xsl:attribute>
											<xsl:value-of select="Title" />
										</a>
										</td>
									</tr>

								</xsl:for-each>
								</table>
								</div>
								</div>
							</xsl:otherwise>

						</xsl:choose>
						<!-- ENDE ANNEXE -->


					</body>
				</html>
				</xsl:template>
				<!-- ENDE ROOT NODE -->

				<!-- DEFINTION DER TEMPLATES -->


				<!-- Template für Lower / Uppercase -->
				 <xsl:template name="ToLower">
				                <xsl:param name="inputString"/>
				                <xsl:variable name="smallCase" select="'abcdefghijklmnopqrstuvwxyzäöü'"/>
				                <xsl:variable name="upperCase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ'"/>
				                <xsl:value-of select="translate($inputString,$upperCase,$smallCase)"/>
				 </xsl:template>

				 <xsl:template name="ToUpper">
				                <xsl:param name="inputString"/>
				                <xsl:variable name="smallCase" select="'abcdefghijklmnopqrstuvwxyzäöü'"/>
				                <xsl:variable name="upperCase" select="'ABCDEFGHIJKLMNOPQRSTUVWXYZÄÖÜ'"/>

				                <xsl:value-of select="translate($inputString,$smallCase,$upperCase)"/>

				   </xsl:template>


<!-- DATUMSFORMATE -->

        <xsl:template name="FormatDate">
				        <xsl:param name="DateTime" />
				        <xsl:variable name="mo">
				                        <xsl:value-of select="substring($DateTime,6,2)" />
				        </xsl:variable>
				        <xsl:variable name="day">
				                        <xsl:value-of select="substring($DateTime,9,2)" />
				          </xsl:variable>
				        <xsl:variable name="year">
				                        <xsl:value-of select="substring($DateTime,1,4)" />
				        </xsl:variable>
				        <xsl:variable name="time">
				                        <xsl:value-of select="substring($DateTime,12,8)" />
				        </xsl:variable>
				        <xsl:variable name="hh">
				                        <xsl:value-of select="substring($time,1,2)" />
				        </xsl:variable>
				        <xsl:variable name="mm">
				                        <xsl:value-of select="substring($time,4,2)" />
				        </xsl:variable>
				        <xsl:variable name="ss">
				                        <xsl:value-of select="substring($time,7,2)" />
				        </xsl:variable>
				                <xsl:value-of select="$day"/>
				              	 <xsl:value-of select="'.'"/>
				                <xsl:value-of select="$mo"/>
				               <xsl:value-of select="'.'"/>
				                <xsl:value-of select="$year"/>
				                <xsl:value-of select="', '"/>
				                <xsl:value-of select="$hh"/>
				               <xsl:value-of select="':'"/>
				                <xsl:value-of select="$mm"/>
				                <xsl:value-of select="':'"/>
				                <xsl:value-of select="$ss"/>
				        </xsl:template>

								<xsl:template name="FormatDateShort">
								        <xsl:param name="DateTime" />
								        <xsl:variable name="mo">
								                        <xsl:value-of select="substring($DateTime,6,2)" />
								        </xsl:variable>
								        <xsl:variable name="day">
								                        <xsl:value-of select="substring($DateTime,9,2)" />
								          </xsl:variable>
								        <xsl:variable name="year">
								                        <xsl:value-of select="substring($DateTime,1,4)" />
								        </xsl:variable>
								        <xsl:variable name="time">
								                        <xsl:value-of select="substring($DateTime,12,8)" />
								        </xsl:variable>
								        <xsl:variable name="hh">
								                        <xsl:value-of select="substring($time,1,2)" />
								        </xsl:variable>
								        <xsl:variable name="mm">
								                        <xsl:value-of select="substring($time,4,2)" />
								        </xsl:variable>
								        <xsl:variable name="ss">
								                        <xsl:value-of select="substring($time,7,2)" />
								        </xsl:variable>
								                <xsl:value-of select="$day"/>
								              	 <xsl:value-of select="'.'"/>
								                <xsl:value-of select="$mo"/>
								               <xsl:value-of select="'.'"/>
								                <xsl:value-of select="$year"/>
								        </xsl:template>


</xsl:stylesheet>
